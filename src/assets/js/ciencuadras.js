var s2options_23e1df8f = {"themeCss": ".select2-container--krajee", "sizeCss": "input-sm", "doReset": true, "doToggle": false, "doOrder": false};
var select2_efcb4002 = {"allowClear": true, "theme": "krajee", "width": "100%", "minimumResultsForSearch": Infinity, "placeholder": "DESDE", "language": "es-CO"};

var select2_573a6e0d = {"allowClear": true, "theme": "krajee", "width": "100%", "minimumResultsForSearch": Infinity, "placeholder": "HASTA", "language": "es-CO"};

var s2options_d6851687 = {"themeCss": ".select2-container--krajee", "sizeCss": "", "doReset": true, "doToggle": false, "doOrder": false};
var select2_5e3e3c84 = {"allowClear": true, "theme": "krajee", "width": "100%", "minimumResultsForSearch": Infinity, "placeholder": "SELECCIONE EL RANGO DE M2", "language": "es-CO"};

var select2_f40fcbf7 = {"allowClear": true, "theme": "krajee", "width": "100%", "minimumResultsForSearch": Infinity, "placeholder": "SELECCIONA LA ANTIGUEDAD", "language": "es-CO"};

var select2_4bb1dc03 = {"allowClear": true, "theme": "krajee", "width": "100%", "minimumResultsForSearch": Infinity, "placeholder": "Ordenar Resultados por:", "language": "es-CO"};

var select2_97d687c2 = {"allowClear": true, "width": "200px", "display": "none", "theme": "krajee", "placeholder": "Selecciona tu ciudad...", "language": "es-CO"};



/* Pixeles */
dataLayer = [];

/* Google Tag Manager Producción */
(function (w, d, s, l, i) {
    w[l] = w[l] || [];
    w[l].push({
        'gtm.start': new Date().getTime(),
        event: 'gtm.js'
    });
    var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
    j.async = true;
    j.src = 'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
    f.parentNode.insertBefore(j, f);
})(window, document, 'script', 'dataLayer', 'GTM-MN3K9JV');
/* End Google Tag Manager */

function cierraNav() {
    //alert ("Ud esta abandonando este sitio, su sesion se finalizara");
}




$('#id_ciudad').val(174);
var microformatoInmuebles = '';
var arrayItemsSeleccionados = new Array();
var objectInmuebles = {};
var objectInmueblesComparar = {};
var newArrayInmuebles = {};
var infowindow = null;
var llamada = null;
var llamadaTimeOut = null;
var arrayLocalidades = new Array();
var arrayBarrios = new Array();
var arrayBarriosLabel = new Array();
var arrayTipoInmueble = new Array();
var arrayHabitaciones = new Array();
var arrayBanos = new Array();
var arrayParqueaderos = new Array();
var areaDataArray = new Array("-50 m2",
        "de 51 m2 a 100 m2",
        "de 101 m2 a 150 m2",
        "de 151 m2 a 200 m2",
        "de 201 m2 a 300 m2",
        "de 301 m2 a 400 m2",
        "de 401 m2 a 500 m2",
        "+501 m2");
var antiguedadDataArray = new Array("de 0 a 5 años",
        "de 6 a 10 años",
        "de 11 a 15 años",
        "de 16 a 20 años",
        "de + 20 años");
var idCiudadConsulta = 174;
var tipoTransaccion = 2;

var paginaActual = 1;
var cargarMasInmuebles = 0;
var paginaAnterior = 1;

// $.typeahead({
//     input: "#principal",
//     minLength: 1,
//     maxItemPerGroup: 8,
//     group: true,
//     hint: true,
//     order: "asc",
//     backdrop: {
//         "background-color": "transparent",
//         "opacity": "0.1",
//         "filter": "alpha(opacity=10)"
//     },
//     multiselect: {
//         matchOn: ["id"],
//         cancelOnBackspace: true
//     },
//     emptyTemplate: "No hay resultados para {{query}}",
//     source: {
//         "Localidades": {
//             ajax: {
//                 url: "/site/localidades",
//                 path: "data.localidad",
//                 data: {
//                     id_ciudad: idCiudadConsulta
//                 }
//             }
//         },
//         "Barrios": {
//             ajax: {
//                 url: "/site/localidades",
//                 path: "data.barrio",
//                 data: {
//                     id_ciudad: idCiudadConsulta
//                 }
//             }
//         },
//         "Habitaciones": {
//             ajax: {
//                 url: "/site/habitaciones",
//                 path: "data.habitaciones"
//             }
//         },
//         "Baños": {
//             ajax: {
//                 url: "/site/banos",
//                 path: "data.banos"
//             }
//         },
//         "Tipo Inmueble": {
//             ajax: {
//                 url: "/site/tipo-inmueble",
//                 path: "data.inmueble"
//             }
//         },
//         "Parqueaderos": {
//             ajax: {
//                 url: "/site/parqueaderos",
//                 path: "data.parqueaderos",
//             }
//         },
//         "Caracteristicas": {
//             ajax: {
//                 url: "/site/caracteristicas",
//                 path: "data.caracteristicas",
//             }
//         },
//         "Precios Desde": {
//             ajax: {
//                 url: "/site/precios-desde",
//                 path: "data.preciosDesde",
//                 data: {
//                     tipoTransaccion: tipoTransaccion
//                 }
//             }
//         },
//         "Precios Hasta": {
//             ajax: {
//                 url: "/site/precios-hasta",
//                 path: "data.preciosHasta",
//                 data: {
//                     tipoTransaccion: tipoTransaccion
//                 }
//             }
//         },
//         "Antigüedad": {
//             ajax: {
//                 url: "/site/antiguedad",
//                 path: "data.antiguedad"
//             }
//         },
//         "Area": {
//             ajax: {
//                 url: "/site/area",
//                 path: "data.area"
//             }
//         },
//     },
//     callback: {
//         onClick: function (node2, a2, item2, event2) {
//             console.log(item2)
//             arrayItemsSeleccionados.push(item2);
//             if (item2.group == 'Localidades') {
//                 setearValorLocalidadSelect(item2);
//             }
//             if (item2.group == 'Barrios') {
//                 setearValorBarrioSelect(item2);
//             }
//             if (item2.group == 'Precios Desde') {
//                 $("#precio_desde").val(item2.id).trigger("change");
//                 document.getElementById('precio_desde').value = item2.id;
//                 accionPrecioDesde();
//             }
//             if (item2.group == 'Precios Hasta') {
//                 $("#precio_hasta").val(item2.id).trigger("change");
//                 document.getElementById('precio_hasta').value = item2.id;
//                 accionPrecioHasta();
//             }
//             if (item2.group == 'Habitaciones') {
//                 arrayHabitaciones = agregarItemSelectPrincipal(arrayHabitaciones, item2.value, "hab", "Habitaciones: ")
//                 $('#num_habitaciones').val(arrayHabitaciones);
//             }
//             if (item2.group == 'Baños') {
//                 arrayBanos = agregarItemSelectPrincipal(arrayBanos, item2.value, "Banos", "Baños: ")
//                 $('#num_banos').val(arrayBanos);
//             }
//             if (item2.group == 'Tipo Inmueble') {
//                 setearValorTipoInmuebleSelect(item2);
//             }
//             if (item2.group == 'Area') {
//                 $("#inmueblesearch-area_construida").val(item2.id).trigger("change");
//                 document.getElementById("inmueblesearch-area_construida").value = item2.id;
//                 setearAreaConstruida(item2.id);
//             }
//             if (item2.group == 'Antigüedad') {
//                 $("#inmueblesearch-antiguedad").val(item2.id).trigger("change");
//                 document.getElementById("inmueblesearch-antiguedad").value = item2.id;
//                 setearAntiguedad(item2.id);
//             }
//             if (item2.group == 'Parqueaderos') {
//                 arrayParqueaderos = agregarItemSelectPrincipal(arrayParqueaderos, item2.value, "parqueaderos", "Parqueaderos: ")
//                 $('#num_parqueaderos').val(arrayParqueaderos);
//             }
//             if (item2.group == 'Caracteristicas') {
//                 if (item2.id == 'asensor') {
//                     $('#num_ascensores').val(1);
//                     $(".bootstrap-tagsinput").prepend('<span class="tag label label-info" data-id="asensor" valor="Asensor" id=mAsensor > Asensor <span data-role="remove"></span></span>');
//                     document.getElementById('ascensorDiv').checked = true;
//                 } else if (item2.id == 'deposito') {
//                     $('#num_depositos').val(1);
//                     $(".bootstrap-tagsinput").prepend('<span class="tag label label-info" data-id="depositos" valor="Depositos" id=mDeporsitos > Deposito <span data-role="remove"></span></span>');
//                     document.getElementById('num_depositosDiv').checked = true;
//                 } else if (item2.id == 'vigilancia') {
//                     $('#vigilancia').val(1);
//                     $(".bootstrap-tagsinput").prepend('<span class="tag label label-info" data-id="vigilancia" valor="vigilancia" id=mVigilancia > Vigilancia <span data-role="remove"></span></span>');
//                     document.getElementById('vigilanciaDiv').checked = true;
//                 } else if (item2.id == 'gimnasio') {
//                     $('#gimnasio').val(1);
//                     $(".bootstrap-tagsinput").prepend('<span class="tag label label-info" data-id="gimnasio" valor="gimnasio" id=mGimnasio > Gimnasio <span data-role="remove"></span></span>');
//                     document.getElementById('gimnasioDiv').checked = true;
//                 } else if (item2.id == 'infantil') {
//                     $('#zona_infantil').val(1);
//                     $(".bootstrap-tagsinput").prepend('<span class="tag label label-info" data-id="infantil" valor="Zonas Infantiles" id=mInfantil > Zonas Infantiles <span data-role="remove"></span></span>');
//                     document.getElementById('zonaInfantil').checked = true;
//                 } else if (item2.id == 'salon') {
//                     $('#salon_comunal').val(1);
//                     $(".bootstrap-tagsinput").prepend('<span class="tag label label-info" data-id="salon" valor="Salon Comunal" id=mSalon > Salon Comunal <span data-role="remove"></span></span>');
//                     document.getElementById('salonComunalDiv').checked = true;
//                 } else if (item2.id == 'piscina') {
//                     $('#piscina_privada').val(1);
//                     $(".bootstrap-tagsinput").prepend('<span class="tag label label-info" data-id="piscina" valor="piscina" id=mPiscina > Piscina <span data-role="remove"></span></span>');
//                     document.getElementById('psicinaDiv').checked = true;
//                 }
//             }
//             ejecutar();
//         },
//     }
// });



function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    ;
    markers = [];
}

$('body').on('click', '.tag', function (event) {
    categoria = this['attributes']['data-id']['value'];
    if (categoria == "hab") {
        document.getElementById(categoria + 1).checked = false;
        document.getElementById(categoria + 2).checked = false;
        document.getElementById(categoria + 3).checked = false;
        document.getElementById(categoria + 4).checked = false;
        document.getElementById(categoria + 5).checked = false;
        arrayHabitaciones = new Array();
        ;
        $('#num_habitaciones').val(arrayHabitaciones);
    } else if (categoria == "Banos") {
        document.getElementById(categoria + 1).checked = false;
        document.getElementById(categoria + 2).checked = false;
        document.getElementById(categoria + 3).checked = false;
        document.getElementById(categoria + 4).checked = false;
        document.getElementById(categoria + 5).checked = false;
        arrayBanos = new Array();
        ;
        $('#num_banos').val(arrayBanos);
    } else if (categoria == "parqueaderos") {
        document.getElementById(categoria + 1).checked = false;
        document.getElementById(categoria + 2).checked = false;
        document.getElementById(categoria + 3).checked = false;
        document.getElementById(categoria + 4).checked = false;
        arrayParqueaderos = new Array();
        ;
        $('#num_parqueaderos').val(arrayParqueaderos);
    } else if (categoria == "barrios") {
        idEliminar = this['attributes']['idBarrio']['value'];
        document.getElementById("barrio" + idEliminar).checked = false;
        arrayBarrios = eliminarValorDeArray(arrayBarrios, idEliminar);
        $('#id_barrio').val(arrayBarrios);
    } else if (categoria == "localidades") {
        idEliminar = this['attributes']['idLocalidad']['value'];
        document.getElementById("localidad" + idEliminar).checked = false;
        arrayLocalidades = eliminarValorDeArray(arrayLocalidades, idEliminar);
        $('#id_localidad').val(arrayLocalidades);
    } else if (categoria == "TipoInmueble") {
        idEliminar = this['attributes']['idTipoInmueble']['value'];
        document.getElementById("TipoInmueble" + idEliminar).checked = false;
        arrayTipoInmueble = eliminarValorDeArray(arrayTipoInmueble, idEliminar);
        $('#id_tipo_inmueble').val(arrayTipoInmueble);
    } else if (categoria == "area") {
        $("#inmueblesearch-area_construida").val(this.id).trigger("change");
        document.getElementById("inmueblesearch-area_construida").value = "";
    } else if (categoria == "antiguedad") {
        $("#inmueblesearch-antiguedad").val(this.id).trigger("change");
        document.getElementById("inmueblesearch-antiguedad").value = "";
    } else if (categoria == "precios") {
        $("#precio_desde").val(this.id).trigger("change");
        $("#precio_hasta").val(this.id).trigger("change");
        document.getElementById("precio_desde").value = "";
        document.getElementById("precio_hasta").value = "";
        document.getElementById("rango-precios-inmuebles").value = "";
    } else if (categoria == "asensor") {
        document.getElementById("ascensorDiv").checked = false;
        $('#num_ascensores').val(0);
    } else if (categoria == "depositos") {
        document.getElementById("num_depositosDiv").checked = false;
        $('#num_depositos').val(0);
    } else if (categoria == "vigilancia") {
        document.getElementById("vigilanciaDiv").checked = false;
        $('#vigilancia').val(0);
    } else if (categoria == "gimnasio") {
        document.getElementById("gimnasioDiv").checked = false;
        $('#gimnasio').val(0);
    } else if (categoria == "infantil") {
        document.getElementById("zonaInfantil").checked = false;
        $('#zona_infantil').val(0);
    } else if (categoria == "salon") {
        document.getElementById("salonComunalDiv").checked = false;
        $('#salon_comunal').val(0);
    } else if (categoria == "piscina") {
        document.getElementById("psicinaDiv").checked = false;
        $('#piscina_privada').val(0);
    }
    $(this).remove();
    //ejecutar();
});

function limpiarFiltros() {
    for (var i = 0; i < arrayLocalidades.length; i++) {
        $("#mlocalidad" + arrayLocalidades[i]).remove();
    }
    for (var i = 0; i < arrayBarrios.length; i++) {
        $("#mbarrio" + arrayBarrios[i]).remove();
    }
    $("#mprecio").remove();
    $("#mhab").remove();
    $("#mBanos").remove();
    $("#mparqueaderos").remove();
    $("#mTipoInmueble10").remove();
    $("#mTipoInmueble11").remove();
    $("#mTipoInmueble12").remove();
    $("#mTipoInmueble13").remove();
    $("#mTipoInmueble14").remove();
    $("#mTipoInmueble15").remove();
    $("#mTipoInmueble16").remove();
    $("#mTipoInmueble17").remove();
    $("#mTipoInmueble21").remove();
    $("#mArea").remove();
    $("#mAntiguedad").remove();
    $("#mPiscina").remove();
    $("#mSalon").remove();
    $("#mInfantil").remove();
    $("#mGimnasio").remove();
    $("#mVigilancia").remove();
    $("#mDeporsitos").remove();
    $("#mAsensor").remove();
    $('input[type=checkbox]').attr('checked', false);
    arrayBarrios = new Array();
    $('#id_barrio').val(arrayBarrios);
    arrayLocalidades = new Array();
    $('#id_localidad').val(arrayLocalidades);
    arrayHabitaciones = new Array();
    ;
    $('#num_habitaciones').val(arrayHabitaciones);
    arrayBanos = new Array();
    ;
    $('#num_banos').val(arrayBanos);
    arrayParqueaderos = new Array();
    $('#num_parqueaderos').val(arrayParqueaderos);
    arrayTipoInmueble = new Array();
    $('#id_tipo_inmueble').val(arrayTipoInmueble);
    $('#num_ascensores').val(0);
    $('#num_depositos').val(0);
    $('#vigilancia').val(0);
    $('#gimnasio').val(0);
    $('#zona_infantil').val(0);
    $('#salon_comunal').val(0);
    $('#piscina_privada').val(0);
    document.getElementById("inmueblesearch-area_construida").value = "";
    $("#inmueblesearch-area_construida").val("").trigger("change");
    document.getElementById("inmueblesearch-antiguedad").value = "";
    $("#inmueblesearch-antiguedad").val("").trigger("change");
    document.getElementById("precio_desde").value = "";
    document.getElementById("precio_hasta").value = "";
    document.getElementById("rango-precios-inmuebles").value = "";
    //ejecutar();
    document.getElementById("numcontag").innerHTML = "";
}

function agregarValorComponentePrincipal(id, categoria, texto, arrayAgregarValor) {
    if ($("#m" + categoria).length > 0) {
        valores = "";
        $("#m" + categoria).remove();
        for (var i = 0; i < arrayAgregarValor.length; i++) {
            if (i == 0) {
                valores = arrayAgregarValor[i];
            } else {
                valores = valores + "," + arrayAgregarValor[i];
            }
        }
        texto = texto + valores;
    } else {
        texto = texto + id;
    }

    if (arrayAgregarValor.length > 0) {
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" data-id="' + categoria + '" valor="' + arrayAgregarValor + '" id=m' + categoria + '>' + texto + '<span data-role="remove"></span></span>');
    }
}

function agregarValorComponentePrincipalInicio(categoria, texto, arrayAgregarValor) {
    for (var i = 0; i < arrayAgregarValor.length; i++) {
        if (i == 0) {
            valores = arrayAgregarValor[i];
        } else {
            valores = valores + "," + arrayAgregarValor[i];
        }
    }
    texto = texto + valores;
    $(".bootstrap-tagsinput").prepend(
            '<span class="tag label label-info" data-id="' + categoria + '" valor="' + arrayAgregarValor + '" id=m' + categoria + '>' + texto + '<span data-role="remove"></span></span>');
}


function eliminarValorDeArray(arrayDatos, valorEliminar) {
    arrayTemp = arrayDatos;
    arrayDatos = new Array();
    for (var i = 0; i < arrayTemp.length; i++) {
        if (arrayTemp[i] != valorEliminar) {
            arrayDatos.push(arrayTemp[i]);
        }
    }
    return arrayDatos;
}

function agregarItemSelectPrincipal(arrayItem, id, categoria, texto) {
    agregarItem = true;
    for (var i = 0; i < arrayItem.length; i++) {
        if (arrayItem[i] == id) {
            agregarItem = false;
            break;
        }
    }
    if (agregarItem) {
        arrayItem.push(id);
        agregarValorComponentePrincipal(id, categoria, texto, arrayItem);
        document.getElementById(categoria + id).checked = true;
    }
    return arrayItem;
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function accionPrecioDesde() {
    var precio_desde = parseInt($("#precio_desde").val()) > 0 ? parseInt($("#precio_desde").val()) : '';
    var precio_hasta = parseInt($("#precio_hasta").val()) > 0 ? parseInt($("#precio_hasta").val()) : '';
    if ($("#precio_hasta").val() > 0) {
        if (precio_desde > precio_hasta) {
            alert('El valor del precio máximo debe ser mayor al precio mínimo seleccionado');
            return false;
        }
    }
    $('#rango-precios-inmuebles').val($('#precio_desde').val() + ',' + $('#precio_hasta').val());
    setearValorPrecio();
}

function accionPrecioHasta() {
    var precio_desde = parseInt($("#precio_desde").val()) > 0 ? parseInt($("#precio_desde").val()) : '';
    var precio_hasta = parseInt($("#precio_hasta").val()) > 0 ? parseInt($("#precio_hasta").val()) : '';
    if ($("#precio_hasta").val() > 0) {
        if (precio_desde > precio_hasta) {
            alert('El valor del precio máximo debe ser mayor al precio mínimo seleccionado');
            return false;
        }
    }
    $('#rango-precios-inmuebles').val($('#precio_desde').val() + ',' + $('#precio_hasta').val());
    setearValorPrecio();
}

function setearValorPrecio() {
    preciosID = document.getElementById('rango-precios-inmuebles').value;
    arrayPrecios = preciosID.split(",");
    precioDesde = arrayPrecios[0];
    precioHasta = arrayPrecios[1];
    textoMostrar = "";
    if (precioDesde != "" && precioHasta == "") {
        textoMostrar = "Precio: Desde $" + numberWithCommas(precioDesde);
    } else if (precioDesde != "" && precioHasta != "" && precioDesde != "null" && precioHasta != "null") {
        textoMostrar = "Precio: $" + numberWithCommas(precioDesde) + " a $" + numberWithCommas(precioHasta);
    } else if (precioDesde == "" && precioHasta != "") {
        textoMostrar = "Precio: Hasta $" + numberWithCommas(precioHasta);
    }
    $("#mprecio").remove();
    if (textoMostrar != "") {
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" data-id="precios" id=mprecio >' + textoMostrar + '<span data-role="remove"></span></span>');
    }
}

function setearAreaConstruida(valor) {
    $("#mArea").remove();
    if (areaDataArray[valor] != undefined) {
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" data-id="area" id=mArea> Area ' + areaDataArray[valor] + '<span data-role="remove"></span></span>');
    }

}

function setearAntiguedad(valor) {
    $("#mAntiguedad").remove();
    if (antiguedadDataArray[valor] != undefined) {
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" data-id="antiguedad" id=mAntiguedad> Antiguedad ' + antiguedadDataArray[valor] + '<span data-role="remove"></span></span>');
    }
}

function setearValorLocalidadSelect(item2) {
    agregar = true;
    for (var i = 0; i < arrayBarrios.length; i++) {
        if (arrayBarrios[i] == item2.id) {
            agregar = false;
            break;
        }
    }
    if (agregar) {
        arrayLocalidades.push(item2.id);
        document.getElementById('localidad' + item2.id).checked = true;
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" idLocalidad ="' + item2.id + '"  data-id="localidades" valor="' + item2.display + '" id=mlocalidad' + item2.id + '>' + item2.display + '<span data-role="remove"></span></span>');
    }
    document.getElementById('id_localidad').value = arrayLocalidades;
}

function setearValorBarrioSelect(item2) {
    agregar = true;
    for (var i = 0; i < arrayBarrios.length; i++) {
        if (arrayBarrios[i] == item2.id) {
            agregar = false;
            break;
        }
    }
    if (agregar) {
        arrayBarrios.push(item2.id);
        document.getElementById('barrio' + item2.id).checked = true;
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" idBarrio ="' + item2.id + '"  data-id="barrios" valor="' + item2.display + '" id=mbarrio' + item2.id + '>' + item2.display + '<span data-role="remove"></span></span>');
    }
    document.getElementById('id_barrio').value = arrayBarrios;
}

function setearValorTipoInmuebleSelect(item2) {
    agregar = true;
    for (var i = 0; i < arrayTipoInmueble.length; i++) {
        if (arrayTipoInmueble[i] == item2.id) {
            agregar = false;
            break;
        }
    }
    if (agregar) {
        arrayTipoInmueble.push(item2.id);
        document.getElementById('TipoInmueble' + item2.id).checked = true;
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" idTipoInmueble="' + item2.id + '"  data-id="TipoInmueble" valor="' + item2.display + '" id=mTipoInmueble' + item2.id + '>' + item2.display + '<span data-role="remove"></span></span>');
    }
    document.getElementById('id_tipo_inmueble').value = arrayTipoInmueble;
}


function fitrarSector(filtro) {
    if (filtro.checked) {
        arrayBarrios.push(filtro.value);
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" idBarrio ="' + filtro.value + '" data-id="barrios" valor="' + filtro.name + '" id=mbarrio' + filtro.value + '>' + filtro.name + '<span data-role="remove"></span></span>');
    } else {
        arrayBarrios = eliminarValorDeArray(arrayBarrios, filtro.value);
        $("#mbarrio" + filtro.value).remove();
    }
    document.getElementById('id_barrio').value = arrayBarrios;
    //ejecutar();
}

function fitrarLocalidad(filtro) {
    if (filtro.checked) {
        arrayLocalidades.push(filtro.value);
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" idLocalidad ="' + filtro.value + '" data-id="localidades" valor="' + filtro.name + '" id=mlocalidad' + filtro.value + '>' + filtro.name + '<span data-role="remove"></span></span>');
    } else {
        arrayLocalidades = eliminarValorDeArray(arrayLocalidades, filtro.value);
        $("#mlocalidad" + filtro.value).remove();
    }
    document.getElementById('id_localidad').value = arrayLocalidades;
    //ejecutar();
}


function fitrarHabitaciones(filtro) {
    if (filtro.checked) {
        arrayHabitaciones.push(filtro.value);
        agregarValorComponentePrincipal(filtro.value, "hab", "Habitaciones: ", arrayHabitaciones);
    } else {
        arrayHabitaciones = eliminarValorDeArray(arrayHabitaciones, filtro.value);
        agregarValorComponentePrincipal(filtro.value, "hab", "Habitaciones: ", arrayHabitaciones);
    }
    $('#num_habitaciones').val(arrayHabitaciones);
    //ejecutar();
}

function fitrarBanos(filtro) {
    if (filtro.checked) {
        arrayBanos.push(filtro.value);
        agregarValorComponentePrincipal(filtro.value, "Banos", "Baños: ", arrayBanos);
    } else {
        arrayBanos = eliminarValorDeArray(arrayBanos, filtro.value);
        agregarValorComponentePrincipal(filtro.value, "Banos", "Baños: ", arrayBanos);
    }
    $('#num_banos').val(arrayBanos);
    //ejecutar();
}

function filtrarTipoInmueble(filtro) {
    if (filtro.checked) {
        arrayTipoInmueble.push(filtro.value);
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" idTipoInmueble ="' + filtro.value + '" data-id="TipoInmueble" valor="' + filtro.name + '" id=mTipoInmueble' + filtro.value + '>' + filtro.name + '<span data-role="remove"></span></span>');
    } else {
        arrayTipoInmueble = eliminarValorDeArray(arrayTipoInmueble, filtro.value);
        $("#mTipoInmueble" + filtro.value).remove();
    }
    /*var filtroTiposInmueble = $('#filtroTiposInmueble').children('li');
     var tiposInmuebleChecked = [];
     var originUrl = window.location.origin;
     var pathname = window.location.pathname;
     var completeUrl = originUrl+pathname;
     for(var i = 0; i < filtroTiposInmueble.length; i++) {
     var isChecked = filtroTiposInmueble[i].children[0].children[0].checked;
     if (isChecked) {
     tiposInmuebleChecked.push(filtroTiposInmueble[i].children[0].children[0].name);
     }
     }
     if (tiposInmuebleChecked.length <= 2) {
     if (tiposInmuebleChecked.length == 2 && tiposInmuebleChecked[0] == 'Apartamento' && tiposInmuebleChecked[1] == 'Casa') {
     history.pushState(null, "", "casa+apartamento");
     } else if (tiposInmuebleChecked.length == 1 && tiposInmuebleChecked[0] == 'Apartamento' || tiposInmuebleChecked[0] == 'Casa') {
     tiposInmuebleChecked[0] == 'Apartamento' ? history.pushState(null, "", 'apartamento') : history.pushState(null, "", 'casa');
     } else {
     limpiarUrlPostFilterInmuebleType(completeUrl);
     }
     } else {
     limpiarUrlPostFilterInmuebleType(completeUrl);
     }*/
    document.getElementById('id_tipo_inmueble').value = arrayTipoInmueble;
    //ejecutar();
}

function limpiarUrlPostFilterInmuebleType(completeUrl) {
    if (completeUrl.includes('casa+apartamento')) {
        history.pushState(null, "", completeUrl.replace('casa+apartamento', ''));
    } else if (completeUrl.includes('apartamento')) {
        history.pushState(null, "", completeUrl.replace('apartamento', ''));
    } else if (completeUrl.includes('casa')) {
        history.pushState(null, "", completeUrl.replace('casa', ''));
    }
}

function filtrarParqueaderos(filtro) {
    if (filtro.checked) {
        arrayParqueaderos.push(filtro.value);
        agregarValorComponentePrincipal(filtro.value, "parqueaderos", "Parqueaderos: ", arrayParqueaderos);
    } else {
        arrayParqueaderos = eliminarValorDeArray(arrayParqueaderos, filtro.value);
        agregarValorComponentePrincipal(filtro.value, "parqueaderos", "Parqueaderos: ", arrayParqueaderos);
    }
    $('#num_parqueaderos').val(arrayParqueaderos);
    //ejecutar();
}


function filtrarAsensor(filtroasensor) {
    if (filtroasensor.checked) {
        $('#num_ascensores').val(1);
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" data-id="asensor" valor="Asensor" id=mAsensor > Asensor <span data-role="remove"></span></span>');
    } else {
        $('#num_ascensores').val(0);
        $("#mAsensor").remove();
    }
   // ejecutar();
}

function filtrarDeposito(filtroDeposito) {
    if (filtroDeposito.checked) {
        $('#num_depositos').val(1);
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" data-id="depositos" valor="Depositos" id=mDeporsitos > Deposito <span data-role="remove"></span></span>');
    } else {
        $('#num_depositos').val(0);
        $("#mDeporsitos").remove();
    }
    //ejecutar();
}

function filtrarVigilancia(filtrovigilancia) {
    if (filtrovigilancia.checked) {
        $('#vigilancia').val(1);
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" data-id="vigilancia" valor="vigilancia" id=mVigilancia > Vigilancia <span data-role="remove"></span></span>');
    } else {
        $('#vigilancia').val(0);
        $("#mVigilancia").remove();
    }
    //ejecutar();
}

function filtrarGimnasio(filtrogimnasio) {
    if (filtrogimnasio.checked) {
        $('#gimnasio').val(1);
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" data-id="gimnasio" valor="gimnasio" id=mGimnasio > Gimnasio <span data-role="remove"></span></span>');
    } else {
        $('#gimnasio').val(0);
        $("#mGimnasio").remove();
    }
    //ejecutar();
}

function filtrarInfantil(filtroinfantil) {
    if (filtroinfantil.checked) {
        $('#zona_infantil').val(1);
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" data-id="infantil" valor="Zonas Infantiles" id=mInfantil > Zonas Infantiles <span data-role="remove"></span></span>');
    } else {
        $('#zona_infantil').val(0);
        $("#mInfantil").remove();
    }
    //ejecutar();
}

function filtrarSalonComunal(filtrosalon) {
    if (filtrosalon.checked) {
        $('#salon_comunal').val(1);
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" data-id="salon" valor="Salon Comunal" id=mSalon > Salon Comunal <span data-role="remove"></span></span>');
    } else {
        $('#salon_comunal').val(0);
        $("#mSalon").remove();
    }
    //ejecutar();
}

function filtrarPiscina(filtropiscina) {
    if (filtropiscina.checked) {
        $('#piscina_privada').val(1);
        $(".bootstrap-tagsinput").prepend(
                '<span class="tag label label-info" data-id="piscina" valor="piscina" id=mPiscina > Piscina <span data-role="remove"></span></span>');
    } else {
        $('#piscina_privada').val(0);
        $("#mPiscina").remove();
    }
    //ejecutar();
}

/*Función para la transformación del botón Comparar*/
var isCompararInmueble = [];

function compararInmueble(id_inm) {

    if ($('#mycheck-' + id_inm).prop('checked')) {
        agregarComparar(id_inm);
    } else {
        eliminarComparar(id_inm);
    }
}

function agregarComparar(id_inm) {

    if (Object.keys(objectInmueblesComparar).length <= 3) {
        objectInmueblesComparar[id_inm] = objectInmuebles[id_inm];

        $("#mycheck-" + id_inm).prop('checked', true);

        generarComparador();
        mostrarComparadorInmuebles();

    } else if (Object.keys(objectInmueblesComparar).length > 3) {
        $("#comparador-completo").modal("show");
        $('#mycheck-' + id_inm).prop('checked', false);
    }
}

function eliminarComparar(id_inm) {

    delete objectInmueblesComparar[id_inm];
    isCompararInmueble = [];

    $("#mycheck-" + id_inm).prop('checked', false);
    $(".img-comparar-ok").html('<img />');
    $(".img-comparar-ok").addClass('img-comparar-blanco');
    $(".img-comparar-blanco").removeClass('img-comparar-ok');
    $(".img-comparar-blanco").removeClass('icon360');

    if (Object.keys(objectInmueblesComparar).length >= 1) {
        generarComparador();
        mostrarComparadorInmuebles();
    } else {
        $(".contenedorCompararInmuebles").css('display', 'none');
    }
}

/*Función para generar el Div del comparador de inmuebles*/
function generarComparador(id_inm) {
    htmlCom = '';
    var con = 0;
    //var newArrayInmuebles = [];

    if (objectInmueblesComparar['es_detalle']) {
        delete objectInmueblesComparar['es_detalle'];
    }
    // if (objectInmueblesComparar['transaccion']) {
    // 	delete objectInmueblesComparar['transaccion'];
    // }

    //if (!Object.keys(objectInmueblesComparar).length > 0) {

    //delete objectInmueblesComparar['inmuebles']
    //}		

    /*if ($.isEmptyObject(newArrayInmuebles)) {
     console.log(newArrayInmuebles);
     }else{
     $.each(newArrayInmuebles, function(i, obj){
     if (i=="es_detalle") {
     return false;
     }
     if (i=="transaccion") {
     return false;
     }
     $('#mycheck-'+i).prop('checked',true);
     });
     }*/

    $.each(objectInmueblesComparar, function (i, obj) {

        $('#mycheck-' + i).prop('checked', true);

        if (i == "es_detalle") {
            return false;
        }
        // if (i=="transaccion") {
        // 	return false;
        // }
        // console.log(i);

        con++;
        i = i + 1;
        var id = obj.id;
        var url_foto = obj.campos.url_foto;
        var clase360 = (obj.campos.es_360 ? ' icon360 ' : '');
        var tipoInmueble = obj.campos.tipoInmueble;
        var nombreBarrio = obj.campos.nombreBarrio;
        var nombreCiudad = obj.campos.nombreCiudad;
        var nombreLocalidad = obj.campos.nombreLocalidad;
        var url_inmueble = obj.campos.url_inmueble;

        if (url_foto != "/images/ciencuadras-inmueble-sin-foto.png") {
            htmlCom = '<div class= "img-comparar-ok ' + clase360 + '">' +
                    '<a href="' + url_inmueble + '" class="inm-link">' +
                    '<img src="' + url_foto + '">' +
                    '</a>' +
                    '<div class="foot-description">' +
                    '<span>' + nombreBarrio + '-' + nombreLocalidad + '-' + nombreCiudad + '</span>' +
                    '</div>' +
                    '<button type="button" id="btn-comparar-inm-' + id + '" class="img-close-inm glyphicon glyphicon-remove" onclick="removeInmueble(' + id + ')">' +
                    '</div>';

        } else {
            url_foto = "/images/ciencuadras-inmueble-sin-foto_2.png";
            htmlCom = '<div class="img-comparar-ok ' + clase360 + '">' +
                    '<a href="' + url_inmueble + '" class="inm-link">' +
                    '<img src="' + url_foto + '">' +
                    '</a>' +
                    '<div class="foot-description">' +
                    '<span>' + nombreBarrio + '-' + nombreLocalidad + '-' + nombreCiudad + '</span>' +
                    '</div>' +
                    '<button type="button" id="btn-comparar-inm-' + id + '" class="img-close-inm glyphicon glyphicon-remove" onclick="removeInmueble(' + id + ')">' +
                    '</div>';
        }
        $("#fotoDiv" + con).html(htmlCom);

    });
}

/*Función para mostrar el div que contiene los inmuebles a seleccionar*/
function mostrarComparadorInmuebles() {
    $(".contenedorCompararInmuebles").css('display', 'block');
}

/*Funcion para ocultar el div de los inmuebles que se estan seleccionando para comparar*/
// $(document).ready(function () {

//     $("#clic_cancelar_limpiar").click(function (event) {

//         isCompararInmueble = [];

//         $.each(objectInmueblesComparar, function (i, obj) {
//             $('#mycheck-' + i).prop('checked', false);
//         });

//         for (var inmuebles in objectInmueblesComparar) {
//             $(".img-comparar-ok").html('<img />');
//             $(".img-comparar-ok").addClass('img-comparar-blanco');
//             $(".img-comparar-blanco").removeClass('img-comparar-ok');
//             $(".img-comparar-blanco").removeClass('icon360');
//             delete objectInmueblesComparar[inmuebles];
//         }

//         $(".contenedorCompararInmuebles").css('display', 'none');

//     });

// });


/*Función para eliminar el inmueble al dar clic en el ícono (x) ubicado en cada inmueble*/
function removeInmueble(id_inm) {

    $("#mycheck-" + id_inm).prop('checked', false);

    $(".img-comparar-ok").html('<img />');
    $(".img-comparar-ok").addClass('img-comparar-blanco');
    $(".img-comparar-blanco").removeClass('img-comparar-ok');
    $(".img-comparar-blanco").removeClass('icon360');
    delete objectInmueblesComparar[id_inm];
    delete isCompararInmueble[id_inm];
    isCompararInmueble = [id_inm];

    if (Object.keys(objectInmueblesComparar).length >= 1) {
        generarComparador();
    } else {
        $(".contenedorCompararInmuebles").css('display', 'none');
    }
}

/*Función para enviar el div de inmuebles a comparar a la página de comparacion de inmuebles -comparar-*/
function enviarComparar() {
    var url = window.location.protocol.concat("//").concat(window.location.host).concat("/inmuebles/almacenar");

    if (Object.keys(objectInmueblesComparar).length > 1) {
        $.ajax({
            type: 'POST',
            url: url,
            data: objectInmueblesComparar,
            dataType: "json",
            success: function (response) {
                window.location.href = "/inmuebles/comparar";
                //console.log(response);
            }, error: function (exception) {
                $("#mostrarmodal").modal("show");
            }
        });
    } else if (Object.keys(objectInmueblesComparar).length <= 1) {
        $("#comparador-incompleto").modal("show");
    }
}

$("#minimizar").click(function () {

    if ($(this).hasClass("icon-arrow-down")) {

        $(this).removeClass('img-minimize-inm glyphicon icon-arrow-down');
        $(this).addClass('img-maximize-inm glyphicon icon-arrow-up');

        $(".letra").text("Desplegar Comparador");

        $(".contenedorCompararInmuebles").css('height', '19px');
        $(".comparador-expandido").css('display', 'none');

    } else if ($(this).hasClass("icon-arrow-up")) {

        $(this).removeClass('img-maximize-inm glyphicon icon-arrow-up');
        $(this).addClass('img-minimize-inm glyphicon icon-arrow-down');

        $(".letra").text("Minimizar Comparador");

        $(".contenedorCompararInmuebles").removeAttr("height");
        $(".contenedorCompararInmuebles").css('height', 'auto');
        $(".comparador-expandido").css('display', 'block');
    }
});


$("#inmueblesearch-id_barrio").on("select2-open", function () {
    $(".select2-container.select2-container-multi.form-control").css('overflow', 'visible');
});

$("#inmueblesearch-id_barrio").on("select2-close", function () {
    $(".select2-container.select2-container-multi.form-control").css('overflow', 'hidden');
});

function ejecutar() {
    if (inm_aplica != "" && inm_aplica != null) {
        document.getElementById('id_inmueble').value = new Array(inm_aplica.substring(0, inm_aplica.length - 1));
    }
    if (llamada && llamada.readyState != 4) {
        llamada.abort();
    }

    var dataConsulta = $('#searchIndex').serializeArray();
    dataConsulta.push({name: 'mapa', value: '1'});
    var tipo = "arriendo";
    dataConsulta.push({name: 'tipo', value: tipo});
    var action = 'inmuebles';
    getInmuebles(dataConsulta, tipo, action);
}
function getInmuebles(data, tipo, action, busquedaPorGeolocalizacion) {
    if (busquedaPorGeolocalizacion == 0) {
        detectarTutorial();
        //$('#progressGeo').show();
    }
    var paginador = "";
    var url = window.location.protocol.concat("//").concat(window.location.host).concat("/inmuebles/inmuebles");
    llamada = $.ajax({
        type: 'POST',
        url: url,
        data: data,
        dataType: "html",
        success: function (response) {
            $('#progressGeo').hide();
            $('#progress').hide();
            jasonINmuebles = JSON.parse(response);
            ordenPubicidad = 0;
            paginador = jasonINmuebles.paginador;
            $(".inmueble-search-pagination").html('');
            $(".inmueble-search-pagination").html(paginador);
            if (jasonINmuebles['inmuebles'] != 0) {
                setResultados(jasonINmuebles, mobile);
                if (cargarMasInmuebles == 0) {
                    setPaginador(paginador);
                } else {
                    cargarMasInmuebles = 0;
                }
                setReportesResultados();
            } else {
                setNoResult();
            }

            if (Object.keys(objectInmueblesComparar).length > 0) {

                var data = Object.values(objectInmueblesComparar);

                for (var i = 0; i < data.length; i++) {

                    var inmueble = data[i];
                    $("#mycheck-" + inmueble.id).prop('checked', true);

                }

            }
        }
    });
}

function crearMicroformato(tipoTransaccion, precio, nombre, url) {
    var precioSinComas = precio.replace(/,/g, '');
    var transaccion = (tipoTransaccion == 'Venta') ? 'Sell' : 'LeaseOut';

    microformatoInmuebles += ' ,{\n';
    microformatoInmuebles += '  "@context"           : "http://schema.org/",\n';
    microformatoInmuebles += '  "@type"              : "Offer",\n';
    microformatoInmuebles += '  "businessFunction"   : "http://purl.org/goodrelations/v1#' + transaccion + '",\n';
    microformatoInmuebles += '  "price"              : "' + precioSinComas + '",\n';
    microformatoInmuebles += '  "priceCurrency"      : "COP",\n';
    microformatoInmuebles += '  "name"               : "' + nombre + '",\n';
    microformatoInmuebles += '  "url"                : "' + url + '"\n';
    microformatoInmuebles += ' }\n';
}

function setReportesResultados() {
    var url = window.location.protocol.concat("//").concat(window.location.host).concat("/inmuebles/realizar-reporte");
    $.ajax({
        type: 'POST',
        url: url,
        dataType: "JSON",
        success: function (response) {
            console.log(response)
        }
    });
}
function getInmueblesTimeOut(data, tipo, action) {
    var paginador = "";
    var url = window.location.protocol.concat("//").concat(window.location.host).concat("/inmuebles/inmuebles");
    llamadaTimeOut = $.ajax({
        type: 'POST',
        url: url,
        data: data,
        dataType: "html",
        success: function (response) {
            $('#progress').hide();
        }, error: function () {
            //$('#progress').show();
        }, timeout: 2500
    });
}

function getMarcadores(data, tipo) {
    var paginador = "";
    var url = window.location.protocol.concat("//").concat(window.location.host).concat("/inmuebles/marcadores");
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function (response) {
            jasonINmuebles = JSON.parse(response);
            if (jasonINmuebles['marcadores'] != 0) {
                /* Pinta los marcadores de las otras páginas */
                $.each(jasonINmuebles['marcadores'], function (i, obj) {
                    pintaMarcador(obj.inmueble, false);
                });
            }
        }
    });
}

function getInmuebles3(data, tipo, action) {
    const promise = new Promise(function (resolve, reject) {
        setTimeout(function () {
            var paginador = "";
            var url = window.location.protocol.concat("//").concat(window.location.host).concat("/inmuebles/inmuebles");
            $.ajax({
                type: 'POST',
                url: url,
                data: data,
                dataType: "html",
                success: function (response) {
                    jasonINmuebles = JSON.parse(response);
                    ordenPubicidad = 0;
                    paginador = jasonINmuebles.paginador;
                    $(".inmueble-search-pagination").html('');
                    $(".inmueble-search-pagination").html(paginador);
                    if (jasonINmuebles['inmuebles'] != 0) {
                        setResultados(jasonINmuebles, mobile);
                        setPaginador(paginador);
                        resolve(jasonINmuebles);
                        setReportesResultados();
                    } else {
                        setNoResult();
                    }
                }
            });

        }, 0);
    })
    return promise;
}

function getInmueblesArea2(data, tipo, action) {

    getInmueblesArea(data, tipo, action).then(function (data_ids) {
        if (data_ids.arraySearch.InmuebleSearch.id[0].length > 0) {

            if (inm_aplica != "") {
                document.getElementById('id_inmueble').value = new Array(inm_aplica.substring(0, inm_aplica.length - 1));
            }
            var dataConsulta = $('#searchIndex').serializeArray();
            getInmuebles3(dataConsulta, tipo, action).then(function (jasonINmuebles) {
                for (i = 2; i < 10; i++) {
                    data_ids.page = i;
                    getMarcadores(data_ids, tipo);
                }
            });
        } else if (figures.length <= 1) {
            setNoResult();
        }
    });
}

var inm_aplica = '';
function getInmueblesArea(data, tipo, action) {

    const promise = new Promise(function (resolve, reject) {
        setTimeout(function () {
            var area_dibujo;

            //Consulta el area de la última figura
            figure = figures[figures.length - 1];
            area_dibujo = google.maps.geometry.spherical.computeArea(figure.getPath().getArray());
            var url = window.location.protocol.concat("//").concat(window.location.host).concat("/inmuebles/get-inmuebles-cercanos-rend");
            var data2 = $('#searchIndex').serializeArray();
            data2.push({name: 'latitud', value: data.inicio_latitud});
            data2.push({name: 'longitud', value: data.inicio_longitud});
            data2.push({name: 'tipoTransaccion', value: data.arraySearch.InmuebleSearch.id_tipo_transaccion});
            data2.push({name: 'area_dibujo', value: area_dibujo});

            var long = data.inicio_longitud;
            var lati = data.inicio_latitud;

            dataLayer.push({
                'Boton': 'lapiz-sector',
                'sector': 'sector-latitud ' + lati + ' longitud ' + long,
                'event': 'btn-lapiz-sector'
            });


            //Consulta los inmuebles cercanos al punto inicial y despues compara si se encuentran en el dibujo
            $.ajax({
                type: 'POST',
                url: url,
                data: data2,
                dataType: "html",
                success: function (response) {
                    jasonINmuebles = JSON.parse(response);
                    $.each(jasonINmuebles, function (i, obj) {
                        var puntoa = new google.maps.LatLng(obj.latitud, obj.longitud);
                        lines.forEach(function (valor, indice, array) {
                            if (puntoa != null && google.maps.geometry.poly.containsLocation(puntoa, lines[indice])) {
                                //inmueblesEnArea.inmuebles[i]=obj;
                                inm_aplica = inm_aplica + obj.id + ',';
                            }
                        });
                    });
                    //inm_aplica = inm_aplica.substring(0,inm_aplica.length-1);
                    data.arraySearch.InmuebleSearch.id = new Array(inm_aplica.substring(0, inm_aplica.length - 1));
                    resolve(data);
                }
            });
        }, 1);
    })
    return promise;
}

function setPaginador(datosPaginador) {
    var html = '';
    var total_paginas = datosPaginador.total_paginas;
    var pageNum = datosPaginador.pageActual + 1;
    var empezarPag = 1;
    html = html + '<div aria-label="Page navigation">'
    html = html + '	<ul class="pagination" id="paginate">'
    if (total_paginas > 10) {
        total_paginas = 10;
        if (pageNum > 5) {
            total_paginas = total_paginas + (pageNum - 5);
            empezarPag = pageNum - 4;
        }
    }
    if (pageNum != 1) {
        pageAnterior = pageNum - 1;
    } else {
        pageAnterior = pageNum;
    }
    if (pageNum != total_paginas) {
        pageNext = pageNum + 1;
    } else {
        pageNext = pageNum;
    }
    html = html + '<li><a  aria-label="Previous" onclick="paginar(this)" data="' + pageAnterior + '">'
    html = html + '	<span style="cursor: pointer" aria-hidden="true">«</span></a>'
    html = html + '</li>'
    for (i = empezarPag; i <= total_paginas; i++) {
        if (pageNum == i) {
            html = html + '<li style="cursor: pointer" class="active"  id="pagina' + i + '"><a>' + i + '</a></li>'
        } else {
            html = html + '<li style="cursor: pointer" data="' + i + '" id="pagina' + i + '" onclick="paginar(this)"><a>' + i + '</a></li>'
        }
        if (datosPaginador.total_paginas == i) {
            break;
        }
    }
    html = html + '<li><a  aria-label="Next" onclick="paginar(this)" data="' + pageNext + '">'
    html = html + '<span style="cursor: pointer" aria-hidden="true">»</span>'
    html = html + '</a></li>'
    html = html + '</ul>'
    html = html + '</div>'
    $("#paginador").html('');
    $("#paginador").append(html);
    html = '';

    document.getElementById('content-product').scrollTop = 0;
    $('body, html').animate({scrollTop: '0px'}, 300);
    $("#id_inmueble").focus();
//$('.container-fluid').slideUp(300);

    $('#main').focus();
    $("#precio_order").focus();
}
function paginar(pagina) {
    paginaActual = $(pagina).attr('data');
    elemento = document.getElementById('pagina' + paginaActual);
    if (elemento != null) {
        $(elemento).addClass("active");
    }
    elemento = document.getElementById('pagina' + paginaAnterior);
    if (elemento != null) {
        $(elemento).removeClass("active");
    }
    paginaAnterior = paginaActual;
    $(pagina).addClass("active");
    //$('#progress').show();
    if (inm_aplica != "") {
        document.getElementById('id_inmueble').value = new Array(inm_aplica.substring(0, inm_aplica.length - 1));
    }
    if (llamada && llamada.readyState != 4) {
        llamada.abort();
    }
    var dataConsulta = $('#searchIndex').serializeArray();
    dataConsulta.push({name: 'mapa', value: '1'});
    dataConsulta.push({name: 'page', value: paginaActual});
    var tipo = "arriendo";
    dataConsulta.push({name: 'tipo', value: tipo});
    var action = 'inmuebles';
    getInmuebles(dataConsulta, tipo, action);
}

function cargarmas(pagina) {
    cargarMasInmuebles = 1;
    //$('#progress').show();
    paginaActual = paginaActual + 1;
    if (llamada && llamada.readyState != 4) {
        llamada.abort();
    }
    var dataConsulta = $('#searchIndex').serializeArray();
    dataConsulta.push({name: 'mapa', value: '1'});
    dataConsulta.push({name: 'page', value: paginaActual});
    var tipo = "arriendo";
    dataConsulta.push({name: 'tipo', value: tipo});
    var action = 'inmuebles';
    getInmuebles(dataConsulta, tipo, action);
}


function setResultados(jsonData, mobile) {
    //if (figures.length <= 1 && integra_map){
    //if (integra_map){
    clearMarkers();
    //}
    var html = '';
    j = 0;
    ordenPubicidad = 0;
    microformatoInmuebles = '';
    if (jsonData.inmuebles.length < 20) {
        $('#cargarMas').hide();
    } else {
        $('#cargarMas').show();
    }
    $.each(jsonData.inmuebles, function (i, obj) {
        objectInmuebles[obj.id] = obj;
        j = j + 1;
        var id = obj.id;
        var idInmueble = obj.campos.idInmueble;
        var tipoTransaccion = obj.campos.tipoTransaccion;
        var certificado = obj.campos.certificado;
        var tipoInmueble = obj.campos.tipoInmueble;
        var url_foto = obj.campos.url_foto;
        var es_360 = obj.campos.es_360;
        var cant_banos = obj.campos.cant_banos;
        var cant_habitaciones = obj.campos.cant_habitaciones;
        var favorito = obj.campos.favorito;
        var precioMostrar = obj.campos.precioMostrar;
        var url_inmueble = obj.campos.url_inmueble;
        var area = obj.campos.area;
        var usuario = obj.campos.usuario;
        var nombreBarrio = obj.campos.nombreBarrio;
        var nombreCiudad = obj.campos.nombreCiudad;
        var nombreLocalidad = obj.campos.nombreLocalidad;
        var canon_arrendamiento = obj.campos.canon_arrendamiento;
        var lat = obj.campos.latitud;
        var long = obj.campos.longitud;
        var sitio = obj.campos.sitio;
        var matterport = obj.campos.matterport;
//alert(matterport);
        html = html + '<div id=' + idInmueble + ' class="col-xs-12 col-sm-12  col-md-6 col-ml-6 col-lg-4">';
        html = html + '  <div class="content-inner-product col-xs-12">';
        html = html + '    <!-- URL Foto -->';
        if (es_360 == true) {
            html = html + ' <figure onclick="abrirDetalleInmueble(id, `' + url_inmueble + '`, `' + tipoTransaccion + '`)" style="cursor:pointer;" id="inmueble-' + id + '"> ';
            if (certificado == 1) {
                html = html + '			<div class="inmueble-verificado">'
                html = html + '            <div class="inner-verificado">'
                html = html + '              <span class="icon-Certificado-verde"></span>'
                html = html + '            </div>'
                html = html + '         </div>'
            }
            // icon360
            html = html + '    		<div class="content-image" style="background: url(' + url_foto + ')  no-repeat center center;"> </div>'
            html = html + '      </figure>';

        } else {

            html = html + '    <figure onclick="abrirDetalleInmueble(id, `' + url_inmueble + '`, `' + tipoTransaccion + '`)" style="cursor:pointer;" id="inmueble-' + id + '"> ';
            if (certificado == 1) {
                html = html + '			<div class="inmueble-verificado">'
                html = html + '            <div class="inner-verificado">'
                html = html + '              <span class="icon-Certificado-verde"></span>'
                html = html + '            </div>'
                html = html + '         </div>'
            }
            html = html + '    		<div class="content-image" style="background: url(' + url_foto + ')  no-repeat center center;"> </div>'
            html = html + '      </figure>';
        }
        html = html + '    <!-- Precio -->';
        html = html + '    <div class="description col-xs-12" onclick="abrirDetalleInmueble(id, `' + url_inmueble + '`, `' + tipoTransaccion + '`)" style="cursor:pointer;"> ';
        if (matterport == true || es_360 == true) {
            html = html + '		<div class="card-info"><ul>';
        }
        if (matterport == true) {
            html = html + '		<li><span class="icon-matterport"></span></li>';
            /*card-info lista estatica !!!!*/
        }
        if (es_360 == true) {
            html = html + '		<li><span class="icon-360"></span></li>';
            /*card-info lista estatica !!!!*/
        }
        if (matterport == true || es_360 == true) {
            html = html + '		</ul></div>';
        }
        html = html + '      <h3><strong>$' + precioMostrar + '</strong></h3>';
        html = html + '      <p><strong>' + nombreBarrio + ' - ' + nombreCiudad + '</strong></p>';
        html = html + '        <p>' + tipoInmueble + ' en ' + tipoTransaccion + '</p>';
        html = html + '    </div>';
        html = html + '    <!-- INFO Inmueble -->';
        html = html + '    <div class="content-icons col-xs-12" onclick="abrirDetalleInmueble(id, `' + url_inmueble + '`, `' + tipoTransaccion + '`)" style="cursor:pointer;">';
        if (cant_habitaciones != 0) {
            html = html + '      <div class="icon" atl="Habitaciones" longdesc="Habitaciones">';

            html = html + '        <p>' + cant_habitaciones + ' Hab</p>';
            html = html + '      </div> ';
        }
        html = html + '      <!-- Banos -->';
        if (cant_banos != 0) {
            html = html + '      <div class="icon" atl="Baños" longdesc="Baños">';

            html = html + '        <p>' + cant_banos + ' Baños</p> ';
            html = html + '      </div>';
        }
        html = html + '      <!-- Área -->';
        if (area != 0 && area != null && area != "null") {
            html = html + '      <div class="icon" atl="Área-Construida" longdesc="Área Construida">';

            html = html + '        <p>' + area + ' m';
            html = html + '        <sup>2</sup></p>';
            html = html + '      </div>';
        }
        html = html + '<!--    <a class="btn btn-default" href="' + url_inmueble + '"  target="_blank"">Ver Inmueble</a>-->';
        html = html + '    </div>';
        html = html + '	   <div class="comparecheck text-center col-xs-12">';

        html = html + '    ';
        html = html + '<div class="check-favorite col-sm-12 col-md-6">';
        html = html + '  <input onclick="mostrarLogin()" class="favorito" type="checkbox" id="fav-check' + idInmueble + '" >';
        html = html + '  <label for="fav-check' + idInmueble + '">Favorito</label>';
        html = html + '</div>';
        html = html + '    ';

        html = html + '<div class="check-compare hidden-xs hidden-sm col-sm-6">';
        html = html + '<input type="checkbox" id="mycheck-' + id + '" onclick="compararInmueble(' + id + ')">';
        html = html + '<label for="mycheck-' + id + '">Comparar</label>';
        html = html + '</div>';

        html = html + '    </div>';
        html = html + '  </div> ';
        html = html + '</div>	';
        crearMicroformato(tipoTransaccion, precioMostrar, tipoInmueble + ' en ' + tipoTransaccion + ' ' + nombreBarrio + ' - ' + nombreCiudad, url_inmueble)

        if (j == 1 && cargarMasInmuebles == 0) {
            $("#inm-list").html('');
        }
        $("#inm-list").append(html);
        html = '';
        pintaMarcador(objectInmuebles[obj.id].campos, true);
    });
    microformatoInmuebles = microformatoInmuebles.slice(0, -1);
    $("#microformato").append(microformatoInmuebles + '\n]');
}

function pintaMarcador(obj, centrar) {

    if (obj.latitud != null && obj.longitud != null && obj.latitud != "" && obj.longitud != "" && parseFloat(obj.latitud) != 0 && parseFloat(obj.longitud) != 0) {

        var content = '<div class="info-mapa" id="info-map-' + obj.id + '">';

        content = content + '<span><b>' + obj.tipoInmueble + ' - ' + obj.nombreLocalidad + ' - ' + obj.nombreCiudad + '</b></span>';

        //if(canon_arrendamiento != 0){content = content +  '<span>Arriendo: $' + canon_arrendamiento_format + '</span>';}

        if (obj.precioMostrar != 0) {
            content = content + '<span>Precio:  $' + obj.precioMostrar + '</span>';
        }

        content = content + '<a href="' + obj.url_inmueble + '"><img src="' + obj.url_foto + '"></a>';

        content = content + '<div class="info-map-detail">';

        //if(codigo != "" && codigo != 0) {content = content + 'Código: ' + codigo  + '<br>';}

        if (obj.cant_habitaciones != 0) {
            content = content + '<span class="icon-habitacionprincipal iconmap"></span>' + '<p class="texticonmap">' + obj.cant_habitaciones + ' Habitaciones' + '</p>';
        }

        if (obj.cant_banos != 0) {
            content = content + '<span class="icon-bano iconmap"></span>' + '<p class="texticonmap">' + obj.cant_banos + ' Baño (s)' + '</p>';
        }

        content = content + '<a href="' + obj.url_inmueble + '" target="_blank"><p class="btn btn-primario maptip">Ver Inmueble</p></a>';

        var ub = new google.maps.LatLng(parseFloat(obj.latitud), parseFloat(obj.longitud));
        var marker = new google.maps.Marker({
            position: ub,
            map: map,
            icon: '/images/marker_2.png'
        });

        markers.push(marker);
        marker.addListener('click', function () {
            addEvenMarkers(this, content);
        });
        if (centrar) {
            map.panTo(ub);
        }
    }
}

function addEvenMarkers(marker, content) {
    if (infowindow != null) {
        infowindow.close();
    }

    infowindow = new google.maps.InfoWindow({content: content});
    infowindow.open(map, marker);
}


function setNoResult() {
    clearMarkers();
    $('#cargarMas').hide();
    var html = '';
    html = html + '<div class="divNoResult">';
    html = html + '<h2>Sigue buscando…</h2>';
    html = html + '<h3>Por ahora ningún inmueble coincide con tu búsqueda, intenta lo siguiente:</h3>';
    html = html + '<ul class="resultado-busqueda">';
    html = html + '<li>Selecciona otra ubicación.</li>';
    html = html + '<li>Modifica alguna opción de búsqueda. </li>';
    html = html + '<li>Haz click en la opción (Guardar búsqueda) y te avisaremos cuando encontremos el inmueble que quieres. </li>';
    html = html + '</ul>';
    html = html + '</div>';
    $("#inm-list").html('');
    $("#paginador").html('');
    $("#inm-list").append(html);
    $("#microformato").append(']');
    html = '';
    /*
     try{
     $('.mapaLateral').each(function() {
     // create map
     map = new_map($(this));
     });
     google.maps.event.addListener(map, "idle", function(){
     google.maps.event.trigger(map, 'resize');
     });
     $(window).resize(function() {
     google.maps.event.trigger(map, "resize");
     });
     }catch(error){
     }
     */

}

function new_map($el) {

    var args = {

        zoom: 12,
        disableDefaultUI: true,
        streetViewControl: true,
        zoomControl: true,
        center: new google.maps.LatLng(4.6117576, -74.0711244),
        /*
         zoom: 15,
         center: new google.maps.LatLng(0, 0),
         mapTypeControl: false,
         panControl: false,
         scrollwheel: false,
         streetViewControl: false,
         zoomControlOptions: {
         style: google.maps.ZoomControlStyle.SMALL,
         position: google.maps.ControlPosition.RIGHT_CENTER
         },     */
    };

    // create map
    map = new google.maps.Map($el[0], args);
    map.addListener('mousedown', function (event) {
        if (state.drawing) {
            //if (!figure) {
            state.isDrawing = true;
            line = new google.maps.Polyline({
                map: map
            });
            var lineStyle = state.satelite ? lineStyles.light : lineStyles.dark;
            line.setOptions(lineStyle);
            line.getPath().push(event.latLng);
            //}
        }

    });

    map.addListener('mousemove', function (event) {
        if (state.drawing && state.isDrawing) {
            line.getPath().push(event.latLng);
        }
    });

    document.getElementById('mapaLateral').addEventListener('touchend', function (event) {
        if (state.drawing && state.isDrawing) {
            state.isDrawing = false;
            state.drawing = false;
            var figureStyle = state.satelite ? figureStyles.light : figureStyles.dark;
            figure = new google.maps.Polygon({});
            figure.setPath(line.getPath());
            figure.setOptions(figureStyle);
            figure.setMap(map);
            //console.log(line.getPath().getArray()[0].lat());
            //console.log(line.getPath().getArray()[0].lng());

            figures.push(figure);

            line.setMap(null);
            figureDone = true;
            var mapOptionsEnabled = {
                draggable: true
            };
            lines.push(line);
            map.setOptions(mapOptionsEnabled);
            var dataIndex = {tipo: 'arriendo',
                page: 1,
                arraySearch: {"InmuebleSearch": {"id_ciudad": "174", "id_tipo_transaccion": 2, "activo": 0}},
                arraySearchRequest: {"InmuebleSearch": {"id_ciudad": "174"}},
                inicio_latitud: line.getPath().getArray()[0].lat(),
                inicio_longitud: line.getPath().getArray()[0].lng(), }
            getInmueblesArea2(dataIndex, "", "inmuebles/inmuebles");
            if (figures.length > 0) {
                enableScroll();
                $("#btn-borrar-dibujo").show();
            }

        }
    });

    /*var centerControlDiv = document.createElement('div');
     var centerControl = new CenterControl(centerControlDiv, map, centerDefault);

     centerControlDiv.index = 100;
     centerControlDiv.style['margin-top'] = '10px';
     //centerControlDiv.style['position'] = 'absolute';
     map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);*/

    // return
    return map;
}

/*function center_map(map) {
 // vars
 bounds = new google.maps.LatLngBounds();
 // loop through all markers and create bounds
 $.each(map.markers, function(i, marker) {
 var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
 bounds.extend(latlng);
 });
 // only 1 marker?
 if (map.markers.length == 1) {
 // set center of map
 map.setCenter(bounds.getCenter());
 map.setZoom(16);
 } else {
 // fit to bounds
 map.fitBounds(bounds);
 }
 }*/

function vistaMapa() {
    $('.mapaLateral').css("width", '100%');
    map.setZoom(9);
}
$(document).ready(function ()
{
    try {
        $('.mapaLateral').each(function () {
            // create map
            map = new_map($(this));
        });
        google.maps.event.addListener(map, "idle", function () {
            google.maps.event.trigger(map, 'resize');
        });
        $(window).resize(function () {
            google.maps.event.trigger(map, "resize");
        });
    } catch (error) {
        //console.log(error);
    }
    //google.maps.event.trigger(map, 'resize');
    $("#btn-borrar-dibujo").hide();
    if (screen.width <= 767) {
        mobile = true;
    } else {
        mobile = false;
    }
    if (1 != 0) {
        //$('#progress').show();
    }
    var dataIndex = {arraySearch: {"InmuebleSearch": {"id_ciudad": "174", "id_tipo_transaccion": 2, "activo": 0}},
        page: 1,
        arraySearchRequest: {"InmuebleSearch": {"id_ciudad": "174"}}}
    //getInmuebles(dataIndex, "", "inmuebles/inmuebles", 1);

    /*$('.infinite_scroll').bind("click",function(e)
     {
     //getInmueblesMobile(dataIndex,"","inmuebles");
     })*/
});

//var json_marcadores = ;
//console.log('Cantidad de registros en mapa = '+json_marcadores.length);
var mobile = false;
var figures = [];
var lines = [];
var map = null;
var bounds = null;
var centerDefault = {lat: 4.61666, lng: -74.09996};
var controlDiv = document.createElement('div');
var state = {
    satelite: false,
    drawing: false,
    isDrawing: false,
    figure: false,
    map: true,
    mapPropertyDetail: false
}
var figure;
var line;
var myPosition;
var noPoi = [{
        featureType: "poi",
        stylers: [
            {visibility: "off"}
        ]
    }];
var figureStyles = {
    light: {
        strokeColor: '#009AD6',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#009AD6',
        fillOpacity: 0.35
    },
    dark: {
        strokeColor: '#009AD6',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#009AD6',
        fillOpacity: 0.35
    }
}
var lineStyles = {
    light: {
        strokeColor: '#009AD6',
        strokeWeight: 2
    },
    dark: {
        strokeColor: '#009AD6',
        strokeWeight: 2
    }
};

/**
 * The CenterControl adds a control to the map that recenters the map on
 * Chicago.
 * @constructor
 * @param {!Element} controlDiv
 * @param {!google.maps.Map} map
 * @param {?google.maps.LatLng} center
 */
function CenterControl(controlDiv, map, center) {
    // We set up a variable for this since we're adding event listeners
    // later.
    /*var control = this;

     // Set the center property upon construction
     control.center_ = center;
     controlDiv.style.clear = 'both';

     // Set CSS for the control border
     var divDubujar = document.createElement('div');
     divDubujar.id = 'divDubujar';
     divDubujar.title = 'Click para Dibujar';
     controlDiv.appendChild(divDubujar);

     // Set CSS for the control interior
     var btnDibujar = document.createElement('div');
     btnDibujar.id = 'btn-dibujar';
     btnDibujar.innerHTML = '<span class="icon-draw-map"></span>';
     divDubujar.appendChild(btnDibujar);

     // Set CSS for the setCenter control border
     var divBorrar = document.createElement('div');
     divBorrar.id = 'divBorrar';

     divBorrar.title = 'Click para borrar dibujo';
     controlDiv.appendChild(divBorrar);

     // Set CSS for the control interior
     var btnBorrar = document.createElement('div');
     btnBorrar.id = 'btn-borrar-dibujo';
     btnBorrar.style = 'display: none;';
     btnBorrar.innerHTML = '<span class="icon-remove"></span>';
     divBorrar.appendChild(btnBorrar);*/

    // Set up the click event listener for 'Center Map': Set the center of
    // the map
    // to the current center of the control.
    var btnDibujar = document.getElementById("btn-dibujar");
    btnDibujar.addEventListener('click', function () {
        //$("#btn-borrar-dibujo").show();
        //$("#btn-dibujar").hide();
        //disableScroll();
        iniciarDibujo();
    });

    function preventDefault(e) {
        e = e || window.event;
        if (e.preventDefault)
            e.preventDefault();
        e.returnValue = false;
    }

    function preventDefaultForScrollKeys(e) {
        if (keys[e.keyCode]) {
            preventDefault(e);
            return false;
        }
    }

    function enableScroll() {
        if (window.removeEventListener)
            window.removeEventListener('DOMMouseScroll', preventDefault, false);
        window.onmousewheel = document.onmousewheel = null;
        window.onwheel = null;
        window.ontouchmove = null;
        document.onkeydown = null;
    }
    function disableScroll() {
        if (window.addEventListener) // older FF
            window.addEventListener('DOMMouseScroll', preventDefault, false);
        window.onwheel = preventDefault; // modern standard
        window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
        window.ontouchmove = preventDefault; // mobile
        document.onkeydown = preventDefaultForScrollKeys;
    }

    // Set up the click event listener for 'Set Center': Set the center of
    // the control to the current center of the map.
    var btnBorrar = document.getElementById("btn-borrar-dibujo");
    btnBorrar.addEventListener('click', function () {
        //borrar dibujo
        $("#btn-borrar-dibujo").hide();
        $("#btn-dibujar").show();
        borrarDibujo();
    });
}

/**
 * Define a property to hold the center state.
 * @private
 */
CenterControl.prototype.center_ = null;

/**
 * Gets the map center.
 * @return {?google.maps.LatLng}
 */
CenterControl.prototype.getCenter = function () {
    return this.center_;
};

/**
 * Sets the map center.
 * @param {?google.maps.LatLng} center
 */
CenterControl.prototype.setCenter = function (center) {
    this.center_ = center;
};

function initMap() {

    map = new google.maps.Map(document.getElementById('mapaLateral'), {
        zoom: 12,
        disableDefaultUI: true,
        streetViewControl: true,
        zoomControl: true,

    });
    /*
     google.maps.event.addDomListener(window, "resize", function() {
     google.maps.event.trigger(map, "resize");
     });
     */
    /*$(window).resize(function() {
     google.maps.event.trigger(map, "resize");
     });*/

    map.addListener('mousedown', function (event) {
        if (state.drawing) {
            //if (!figure) {
            state.isDrawing = true;
            line = new google.maps.Polyline({
                map: map
            });
            var lineStyle = state.satelite ? lineStyles.light : lineStyles.dark;
            line.setOptions(lineStyle);
            line.getPath().push(event.latLng);
            //}
        }

    });

    map.addListener('mousemove', function (event) {
        if (state.drawing && state.isDrawing) {
            line.getPath().push(event.latLng);
        }
    });

    document.getElementById('mapaLateral').addEventListener('mouseup', function (event) {
        if (state.drawing && state.isDrawing) {
            state.isDrawing = false;
            state.drawing = false;
            var figureStyle = state.satelite ? figureStyles.light : figureStyles.dark;
            figure = new google.maps.Polygon({});
            figure.setPath(line.getPath());
            figure.setOptions(figureStyle);
            figure.setMap(map);
            //console.log(line.getPath().getArray()[0].lat());
            //console.log(line.getPath().getArray()[0].lng());

            figures.push(figure);

            line.setMap(null);
            figureDone = true;
            var mapOptionsEnabled = {
                draggable: true
            };
            lines.push(line);
            map.setOptions(mapOptionsEnabled);
            var dataIndex = {tipo: 'arriendo',
                page: 1,
                arraySearch: {"InmuebleSearch": {"id_ciudad": "174", "id_tipo_transaccion": 2, "activo": 0}},
                arraySearchRequest: {"InmuebleSearch": {"id_ciudad": "174"}},
                inicio_latitud: line.getPath().getArray()[0].lat(),
                inicio_longitud: line.getPath().getArray()[0].lng(), }
            getInmueblesArea2(dataIndex, "", "inmuebles/inmuebles");
            if (figures.length > 0) {
                $("#btn-borrar-dibujo").show();
            }

        }
    });
    // Create the DIV to hold the control and call the CenterControl()
    // constructor
    // passing in this DIV.
    var centerControlDiv = document.createElement('div');
    var centerControl = new CenterControl(centerControlDiv, map, centerDefault);

    centerControlDiv.index = 100;
    centerControlDiv.style['margin-top'] = '10px';
    //centerControlDiv.style['position'] = 'absolute';
    map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
}
function iniciarDibujo() { //alert()
    if (map && figures.length < 3) {
        var mapOptionsDisabled = {
            draggable: false
        };
        map.setOptions(mapOptionsDisabled);
        state.drawing = true;
    } else {
        $("#btn-dibujar").hide();
    }

}

function traeInmuebles() {

    const promise = new Promise(function (resolve, reject) {
        setTimeout(function () {
            var dataIndex = {tipo: 'arriendo', page: 1, arraySearch: {"InmuebleSearch": {"id_ciudad": "174", "id_tipo_transaccion": 2, "activo": 0}}, arraySearchRequest: {"InmuebleSearch": {"id_ciudad": "174"}}}
            resolve(dataIndex)
        }, 0);
    })
    return promise;
}


function borrarDibujo() {

    state.drawing = false;
    removeFigure();
    inm_aplica = '';
    var mapOptionsEnabled = {
        draggable: true
    };
    map.setOptions(mapOptionsEnabled);
    traeInmuebles().then(function (dataIndex) {
        //console.log(dataIndex);
        document.getElementById('id_inmueble').value = new Array(inm_aplica.substring(0, inm_aplica.length - 1));
        var dataConsulta = $('#searchIndex').serializeArray();
        getInmuebles(dataConsulta, "", "inmuebles/inmuebles");
    });

}



//function borrarDibujo(){
/*state.drawing = false;
 removeFigure();
 var mapOptionsEnabled = {
 draggable: true
 };
 map.setOptions(mapOptionsEnabled);
 var dataIndex = { tipo : 'arriendo', page : , arraySearch: , arraySearchRequest:  }
 getInmuebles(dataIndex,"","inmuebles");*/
//}

function removeFigure() {
    drawingLatLngs = [];
    figures.forEach(function (valor, indice, array) {
        figure = figures[indice];
        figure.setPath([]);
        figure.setMap(null);
        figure = null;
        //delete figures[indice];
    });
    figures.splice(0, figures.length);
    lines.splice(0, lines.length);

    /*drawingLatLngs = [];
     if (figure) {
     figure.setPath([]);
     figure.setMap(null);
     figure = null;
     }*/
}

$(document).ready(function () {
    $('#extendermapa').click(function (e) {
        dataLayer.push({
            'Boton': 'expandir-mapa',
            'event': 'btn-expandir-mapa'
        });
    });
});