! function(d) {
    if (!d.Tween || !d.Tween.propHooks) throw new Error("jquery.animateNumber requires jQuery 1.8.0 or higher");
    var l = {
        number_step: function(t, e) {
            var i = Math.floor(t);
            d(e.elem).text(i)
        }
    };
    d.Tween.propHooks.number = {
        set: function(t) {
            if (t.elem.nodeType && t.elem.parentNode) {
                var e = t.elem._animateNumberSetter;
                e || (e = l.number_step), e(t.now, t)
            }
        }
    }, d.animateNumber = {
        number_step_factories: {
            append: function(o) {
                return function(t, e) {
                    var i = Math.floor(t);
                    d(e.elem).prop("number", t).text(i + o)
                }
            },
            separator: function(u, p) {
                return u = u || " ", p = p || 3,
                    function(t, e) {
                        var i = Math.floor(t).toString(),
                            o = d(e.elem);
                        if (i.length > p) {
                            for (var s, n, r, a = i.split("").reverse(), l = [], h = 0, c = Math.ceil(i.length / p); h < c; h++) {
                                for (s = "", r = 0; r < p && (n = h * p + r) != i.length; r++) s += a[n];
                                l.push(s)
                            }
                            i = (i = l.join(u)).split("").reverse().join("")
                        }
                        o.prop("number", t).text(i)
                    }
            }
        }
    }, d.fn.animateNumber = function() {
        for (var t = arguments[0], e = d.extend({}, l, t), i = d(this), o = [e], s = 1, n = arguments.length; s < n; s++) o.push(arguments[s]);
        if (t.number_step) {
            var r = this.each(function() {
                    this._animateNumberSetter = t.number_step
                }),
                a = e.complete;
            e.complete = function() {
                r.each(function() {
                    delete this._animateNumberSetter
                }), a && a.apply(this, arguments)
            }
        }
        return i.animate.apply(i, o)
    }
}(jQuery),
function(e) {
    var t;
    "function" == typeof define && define.amd ? define("jquery-typeahead", ["jquery"], function(t) {
        return e(t)
    }) : "object" == typeof module && module.exports ? module.exports = (void 0 === t && (t = "undefined" != typeof window ? require("jquery") : require("jquery")(void 0)), e(t)) : e(jQuery)
}(function(T) {
    "use strict";
    var i, o = {
            input: null,
            minLength: 2,
            maxLength: !(window.Typeahead = {
                version: "2.10.4"
            }),
            maxItem: 8,
            dynamic: !1,
            delay: 300,
            order: null,
            offset: !1,
            hint: !1,
            accent: !1,
            highlight: !0,
            multiselect: null,
            group: !1,
            groupOrder: null,
            maxItemPerGroup: null,
            dropdownFilter: !1,
            dynamicFilter: null,
            backdrop: !1,
            backdropOnFocus: !1,
            cache: !1,
            ttl: 36e5,
            compression: !1,
            searchOnFocus: !1,
            blurOnTab: !0,
            resultContainer: null,
            generateOnLoad: null,
            mustSelectItem: !1,
            href: null,
            display: ["display"],
            template: null,
            templateValue: null,
            groupTemplate: null,
            correlativeTemplate: !1,
            emptyTemplate: !1,
            cancelButton: !0,
            loadingAnimation: !0,
            filter: !0,
            matcher: null,
            source: null,
            callback: {
                onInit: null,
                onReady: null,
                onShowLayout: null,
                onHideLayout: null,
                onSearch: null,
                onResult: null,
                onLayoutBuiltBefore: null,
                onLayoutBuiltAfter: null,
                onNavigateBefore: null,
                onNavigateAfter: null,
                onEnter: null,
                onLeave: null,
                onClickBefore: null,
                onClickAfter: null,
                onDropdownFilter: null,
                onSendRequest: null,
                onReceiveRequest: null,
                onPopulateSource: null,
                onCacheSave: null,
                onSubmit: null,
                onCancel: null
            },
            selector: {
                container: "typeahead__container",
                result: "typeahead__result",
                list: "typeahead__list",
                group: "typeahead__group",
                item: "typeahead__item",
                empty: "typeahead__empty",
                display: "typeahead__display",
                query: "typeahead__query",
                filter: "typeahead__filter",
                filterButton: "typeahead__filter-button",
                dropdown: "typeahead__dropdown",
                dropdownItem: "typeahead__dropdown-item",
                labelContainer: "typeahead__label-container",
                label: "typeahead__label",
                button: "typeahead__button",
                backdrop: "typeahead__backdrop",
                hint: "typeahead__hint",
                cancelButton: "typeahead__cancel-button"
            },
            debug: !1
        },
        s = {
            from: "Ã£Ã Ã¡Ã¤Ã¢áº½Ã¨Ã©Ã«ÃªÃ¬Ã­Ã¯Ã®ÃµÃ²Ã³Ã¶Ã´Ã¹ÃºÃ¼Ã»Ã±Ã§",
            to: "aaaaaeeeeeiiiiooooouuuunc"
        },
        n = ~window.navigator.appVersion.indexOf("MSIE 9."),
        r = ~window.navigator.appVersion.indexOf("MSIE 10"),
        a = !!~window.navigator.userAgent.indexOf("Trident") && ~window.navigator.userAgent.indexOf("rv:11"),
        l = function(t, e) {
            this.rawQuery = t.val() || "", this.query = t.val() || "", this.selector = t[0].selector, this.deferred = null, this.tmpSource = {}, this.source = {}, this.dynamicGroups = [], this.hasDynamicGroups = !1, this.generatedGroupCount = 0, this.groupBy = "group", this.groups = [], this.searchGroups = [], this.generateGroups = [], this.requestGroups = [], this.result = [], this.tmpResult = {}, this.groupTemplate = "", this.resultHtml = null, this.resultCount = 0, this.resultCountPerGroup = {}, this.options = e, this.node = t, this.namespace = "." + this.helper.slugify.call(this, this.selector) + ".typeahead", this.isContentEditable = void 0 !== this.node.attr("contenteditable") && "false" !== this.node.attr("contenteditable"), this.container = null, this.resultContainer = null, this.item = null, this.items = null, this.comparedItems = null, this.xhr = {}, this.hintIndex = null, this.filters = {
                dropdown: {},
                dynamic: {}
            }, this.dropdownFilter = {
                static: [],
                dynamic: []
            }, this.dropdownFilterAll = null, this.isDropdownEvent = !1, this.requests = {}, this.backdrop = {}, this.hint = {}, this.label = {}, this.hasDragged = !1, this.focusOnly = !1, this.__construct()
        };
    l.prototype = {
        _validateCacheMethod: function(t) {
            var e;
            if (!0 === t) t = "localStorage";
            else if ("string" == typeof t && !~["localStorage", "sessionStorage"].indexOf(t)) return this.options.debug && (G.log({
                node: this.selector,
                function: "extendOptions()",
                message: 'Invalid options.cache, possible options are "localStorage" or "sessionStorage"'
            }), G.print()), !1;
            e = void 0 !== window[t];
            try {
                window[t].setItem("typeahead", "typeahead"), window[t].removeItem("typeahead")
            } catch (t) {
                e = !1
            }
            return e && t || !1
        },
        extendOptions: function() {
            if (this.options.cache = this._validateCacheMethod(this.options.cache), this.options.compression && ("object" == typeof LZString && this.options.cache || (this.options.debug && (G.log({
                    node: this.selector,
                    function: "extendOptions()",
                    message: "Missing LZString Library or options.cache, no compression will occur."
                }), G.print()), this.options.compression = !1)), this.options.maxLength && !isNaN(this.options.maxLength) || (this.options.maxLength = 1 / 0), void 0 !== this.options.maxItem && ~[0, !1].indexOf(this.options.maxItem) && (this.options.maxItem = 1 / 0), this.options.maxItemPerGroup && !/^\d+$/.test(this.options.maxItemPerGroup) && (this.options.maxItemPerGroup = null), this.options.display && !Array.isArray(this.options.display) && (this.options.display = [this.options.display]), this.options.multiselect && (this.items = [], this.comparedItems = [], "string" == typeof this.options.multiselect.matchOn && (this.options.multiselect.matchOn = [this.options.multiselect.matchOn])), this.options.group && (Array.isArray(this.options.group) ? this.options.debug && (G.log({
                    node: this.selector,
                    function: "extendOptions()",
                    message: "options.group must be a boolean|string|object as of 2.5.0"
                }), G.print()) : ("string" == typeof this.options.group ? this.options.group = {
                    key: this.options.group
                } : "boolean" == typeof this.options.group && (this.options.group = {
                    key: "group"
                }), this.options.group.key = this.options.group.key || "group")), this.options.highlight && !~["any", !0].indexOf(this.options.highlight) && (this.options.highlight = !1), this.options.dropdownFilter && this.options.dropdownFilter instanceof Object) {
                Array.isArray(this.options.dropdownFilter) || (this.options.dropdownFilter = [this.options.dropdownFilter]);
                for (var t = 0, e = this.options.dropdownFilter.length; t < e; ++t) this.dropdownFilter[this.options.dropdownFilter[t].value ? "static" : "dynamic"].push(this.options.dropdownFilter[t])
            }
            this.options.dynamicFilter && !Array.isArray(this.options.dynamicFilter) && (this.options.dynamicFilter = [this.options.dynamicFilter]), this.options.accent && ("object" == typeof this.options.accent ? this.options.accent.from && this.options.accent.to && this.options.accent.from.length !== this.options.accent.to.length && this.options.debug && (G.log({
                node: this.selector,
                function: "extendOptions()",
                message: 'Invalid "options.accent", from and to must be defined and same length.'
            }), G.print()) : this.options.accent = s), this.options.groupTemplate && (this.groupTemplate = this.options.groupTemplate), this.options.resultContainer && ("string" == typeof this.options.resultContainer && (this.options.resultContainer = T(this.options.resultContainer)), this.options.resultContainer instanceof T && this.options.resultContainer[0] ? this.resultContainer = this.options.resultContainer : this.options.debug && (G.log({
                node: this.selector,
                function: "extendOptions()",
                message: 'Invalid jQuery selector or jQuery Object for "options.resultContainer".'
            }), G.print())), this.options.maxItemPerGroup && this.options.group && this.options.group.key && (this.groupBy = this.options.group.key), this.options.callback && this.options.callback.onClick && (this.options.callback.onClickBefore = this.options.callback.onClick, delete this.options.callback.onClick), this.options.callback && this.options.callback.onNavigate && (this.options.callback.onNavigateBefore = this.options.callback.onNavigate, delete this.options.callback.onNavigate), this.options = T.extend(!0, {}, o, this.options)
        },
        unifySourceFormat: function() {
            var t, e, i;
            for (t in this.dynamicGroups = [], Array.isArray(this.options.source) && (this.options.source = {
                    group: {
                        data: this.options.source
                    }
                }), "string" == typeof this.options.source && (this.options.source = {
                    group: {
                        ajax: {
                            url: this.options.source
                        }
                    }
                }), this.options.source.ajax && (this.options.source = {
                    group: {
                        ajax: this.options.source.ajax
                    }
                }), (this.options.source.url || this.options.source.data) && (this.options.source = {
                    group: this.options.source
                }), this.options.source)
                if (this.options.source.hasOwnProperty(t)) {
                    if ("string" == typeof(e = this.options.source[t]) && (e = {
                            ajax: {
                                url: e
                            }
                        }), i = e.url || e.ajax, Array.isArray(i) ? (e.ajax = "string" == typeof i[0] ? {
                            url: i[0]
                        } : i[0], e.ajax.path = e.ajax.path || i[1] || null) : "object" == typeof e.url ? e.ajax = e.url : "string" == typeof e.url && (e.ajax = {
                            url: e.url
                        }), delete e.url, !e.data && !e.ajax) return this.options.debug && (G.log({
                        node: this.selector,
                        function: "unifySourceFormat()",
                        arguments: JSON.stringify(this.options.source),
                        message: 'Undefined "options.source.' + t + '.[data|ajax]" is Missing - Typeahead dropped'
                    }), G.print()), !1;
                    e.display && !Array.isArray(e.display) && (e.display = [e.display]), e.minLength = "number" == typeof e.minLength ? e.minLength : this.options.minLength, e.maxLength = "number" == typeof e.maxLength ? e.maxLength : this.options.maxLength, e.dynamic = "boolean" == typeof e.dynamic || this.options.dynamic, e.minLength > e.maxLength && (e.minLength = e.maxLength), this.options.source[t] = e, this.options.source[t].dynamic && this.dynamicGroups.push(t), e.cache = void 0 !== e.cache ? this._validateCacheMethod(e.cache) : this.options.cache, e.compression && ("object" == typeof LZString && e.cache || (this.options.debug && (G.log({
                        node: this.selector,
                        function: "unifySourceFormat()",
                        message: "Missing LZString Library or group.cache, no compression will occur on group: " + t
                    }), G.print()), e.compression = !1))
                }
            return this.hasDynamicGroups = this.options.dynamic || !!this.dynamicGroups.length, !0
        },
        init: function() {
            this.helper.executeCallback.call(this, this.options.callback.onInit, [this.node]), this.container = this.node.closest("." + this.options.selector.container), this.options.debug && (G.log({
                node: this.selector,
                function: "init()",
                message: "OK - Typeahead activated on " + this.selector
            }), G.print())
        },
        delegateEvents: function() {
            var i = this,
                t = ["focus" + this.namespace, "input" + this.namespace, "propertychange" + this.namespace, "keydown" + this.namespace, "keyup" + this.namespace, "search" + this.namespace, "generate" + this.namespace];
            T("html").on("touchmove", function() {
                i.hasDragged = !0
            }).on("touchstart", function() {
                i.hasDragged = !1
            }), this.node.closest("form").on("submit", function(t) {
                if (!i.options.mustSelectItem || !i.helper.isEmpty(i.item)) return i.options.backdropOnFocus || i.hideLayout(), i.options.callback.onSubmit ? i.helper.executeCallback.call(i, i.options.callback.onSubmit, [i.node, this, i.item || i.items, t]) : void 0;
                t.preventDefault()
            }).on("reset", function() {
                setTimeout(function() {
                    i.node.trigger("input" + i.namespace), i.hideLayout()
                })
            });
            var o = !1;
            if (this.node.attr("placeholder") && (r || a)) {
                var e = !0;
                this.node.on("focusin focusout", function() {
                    e = !(this.value || !this.placeholder)
                }), this.node.on("input", function(t) {
                    e && (t.stopImmediatePropagation(), e = !1)
                })
            }
            this.node.off(this.namespace).on(t.join(" "), function(t, e) {
                switch (t.type) {
                    case "generate":
                        i.generateSource(Object.keys(i.options.source));
                        break;
                    case "focus":
                        if (i.focusOnly) {
                            i.focusOnly = !1;
                            break
                        }
                        i.options.backdropOnFocus && (i.buildBackdropLayout(), i.showLayout()), i.options.searchOnFocus && !i.item && (i.deferred = T.Deferred(), i.assignQuery(), i.generateSource());
                        break;
                    case "keydown":
                        8 === t.keyCode && i.options.multiselect && i.options.multiselect.cancelOnBackspace && "" === i.query && i.items.length ? i.cancelMultiselectItem(i.items.length - 1, null, t) : t.keyCode && ~[9, 13, 27, 38, 39, 40].indexOf(t.keyCode) && (o = !0, i.navigate(t));
                        break;
                    case "keyup":
                        n && i.node[0].value.replace(/^\s+/, "").toString().length < i.query.length && i.node.trigger("input" + i.namespace);
                        break;
                    case "propertychange":
                        if (o) {
                            o = !1;
                            break
                        }
                    case "input":
                        i.deferred = T.Deferred(), i.assignQuery(), "" === i.rawQuery && "" === i.query && (t.originalEvent = e || {}, i.helper.executeCallback.call(i, i.options.callback.onCancel, [i.node, t])), i.options.cancelButton && i.toggleCancelButtonVisibility(), i.options.hint && i.hint.container && "" !== i.hint.container.val() && 0 !== i.hint.container.val().indexOf(i.rawQuery) && (i.hint.container.val(""), i.isContentEditable && i.hint.container.text("")), i.hasDynamicGroups ? i.helper.typeWatch(function() {
                            i.generateSource()
                        }, i.options.delay) : i.generateSource();
                        break;
                    case "search":
                        i.searchResult(), i.buildLayout(), i.result.length || i.searchGroups.length && i.options.emptyTemplate && i.query.length ? i.showLayout() : i.hideLayout(), i.deferred && i.deferred.resolve()
                }
                return i.deferred && i.deferred.promise()
            }), this.options.generateOnLoad && this.node.trigger("generate" + this.namespace)
        },
        assignQuery: function() {
            this.isContentEditable ? this.rawQuery = this.node.text() : this.rawQuery = this.node.val().toString(), this.rawQuery = this.rawQuery.replace(/^\s+/, ""), this.rawQuery !== this.query && (this.item = null, this.query = this.rawQuery)
        },
        filterGenerateSource: function() {
            if (this.searchGroups = [], this.generateGroups = [], !this.focusOnly || this.options.multiselect)
                for (var t in this.options.source)
                    if (this.options.source.hasOwnProperty(t) && this.query.length >= this.options.source[t].minLength && this.query.length <= this.options.source[t].maxLength) {
                        if (this.searchGroups.push(t), !this.options.source[t].dynamic && this.source[t]) continue;
                        this.generateGroups.push(t)
                    }
        },
        generateSource: function(t) {
            if (this.filterGenerateSource(), Array.isArray(t) && t.length) this.generateGroups = t;
            else if (!this.generateGroups.length) return void this.node.trigger("search" + this.namespace);
            if (this.requestGroups = [], this.generatedGroupCount = 0, this.options.loadingAnimation && this.container.addClass("loading"), !this.helper.isEmpty(this.xhr)) {
                for (var e in this.xhr) this.xhr.hasOwnProperty(e) && this.xhr[e].abort();
                this.xhr = {}
            }
            for (var i, o, s, n, r, a, l, h = this, c = (e = 0, this.generateGroups.length); e < c; ++e) {
                if (i = this.generateGroups[e], n = (s = this.options.source[i]).cache, r = s.compression, n && (a = window[n].getItem("TYPEAHEAD_" + this.selector + ":" + i))) {
                    r && (a = LZString.decompressFromUTF16(a)), l = !1;
                    try {
                        (a = JSON.parse(a + "")).data && a.ttl > (new Date).getTime() ? (this.populateSource(a.data, i), l = !0, this.options.debug && (G.log({
                            node: this.selector,
                            function: "generateSource()",
                            message: 'Source for group "' + i + '" found in ' + n
                        }), G.print())) : window[n].removeItem("TYPEAHEAD_" + this.selector + ":" + i)
                    } catch (t) {}
                    if (l) continue
                }!s.data || s.ajax ? s.ajax && (this.requests[i] || (this.requests[i] = this.generateRequestObject(i)), this.requestGroups.push(i)) : "function" == typeof s.data ? (o = s.data.call(this), Array.isArray(o) ? h.populateSource(o, i) : "function" == typeof o.promise && function(e) {
                    T.when(o).then(function(t) {
                        t && Array.isArray(t) && h.populateSource(t, e)
                    })
                }(i)) : this.populateSource(T.extend(!0, [], s.data), i)
            }
            return this.requestGroups.length && this.handleRequests(), !!this.generateGroups.length
        },
        generateRequestObject: function(o) {
            var s = this,
                n = this.options.source[o],
                t = {
                    request: {
                        url: n.ajax.url || null,
                        dataType: "json",
                        beforeSend: function(t, e) {
                            s.xhr[o] = t;
                            var i = s.requests[o].callback.beforeSend || n.ajax.beforeSend;
                            "function" == typeof i && i.apply(null, arguments)
                        }
                    },
                    callback: {
                        beforeSend: null,
                        done: null,
                        fail: null,
                        then: null,
                        always: null
                    },
                    extra: {
                        path: n.ajax.path || null,
                        group: o
                    },
                    validForGroup: [o]
                };
            if ("function" != typeof n.ajax && (n.ajax instanceof Object && (t = this.extendXhrObject(t, n.ajax)), 1 < Object.keys(this.options.source).length))
                for (var e in this.requests) this.requests.hasOwnProperty(e) && (this.requests[e].isDuplicated || t.request.url && t.request.url === this.requests[e].request.url && (this.requests[e].validForGroup.push(o), t.isDuplicated = !0, delete t.validForGroup));
            return t
        },
        extendXhrObject: function(t, e) {
            return "object" == typeof e.callback && (t.callback = e.callback, delete e.callback), "function" == typeof e.beforeSend && (t.callback.beforeSend = e.beforeSend, delete e.beforeSend), t.request = T.extend(!0, t.request, e), "jsonp" !== t.request.dataType.toLowerCase() || t.request.jsonpCallback || (t.request.jsonpCallback = "callback_" + t.extra.group), t
        },
        handleRequests: function() {
            var t, h = this,
                c = this.requestGroups.length;
            if (!1 !== this.helper.executeCallback.call(this, this.options.callback.onSendRequest, [this.node, this.query]))
                for (var e = 0, i = this.requestGroups.length; e < i; ++e) t = this.requestGroups[e], this.requests[t].isDuplicated || function(t, r) {
                    if ("function" == typeof h.options.source[t].ajax) {
                        var e = h.options.source[t].ajax.call(h, h.query);
                        if ("object" != typeof(r = h.extendXhrObject(h.generateRequestObject(t), "object" == typeof e ? e : {})).request || !r.request.url) return h.options.debug && (G.log({
                            node: h.selector,
                            function: "handleRequests",
                            message: 'Source function must return an object containing ".url" key for group "' + t + '"'
                        }), G.print()), h.populateSource([], t);
                        h.requests[t] = r
                    }
                    var a, i = !1,
                        l = {};
                    if (~r.request.url.indexOf("{{query}}") && (i || (r = T.extend(!0, {}, r), i = !0), r.request.url = r.request.url.replace("{{query}}", encodeURIComponent(h.query))), r.request.data)
                        for (var o in r.request.data)
                            if (r.request.data.hasOwnProperty(o) && ~String(r.request.data[o]).indexOf("{{query}}")) {
                                i || (r = T.extend(!0, {}, r), i = !0), r.request.data[o] = r.request.data[o].replace("{{query}}", h.query);
                                break
                            }
                    T.ajax(r.request).done(function(t, e, i) {
                        for (var o, s = 0, n = r.validForGroup.length; s < n; s++) o = r.validForGroup[s], (a = h.requests[o]).callback.done instanceof Function && (l[o] = a.callback.done.call(h, t, e, i), Array.isArray(l[o]) && "object" == typeof l[o] || h.options.debug && (G.log({
                            node: h.selector,
                            function: "Ajax.callback.done()",
                            message: "Invalid returned data has to be an Array"
                        }), G.print()))
                    }).fail(function(t, e, i) {
                        for (var o = 0, s = r.validForGroup.length; o < s; o++)(a = h.requests[r.validForGroup[o]]).callback.fail instanceof Function && a.callback.fail.call(h, t, e, i);
                        h.options.debug && (G.log({
                            node: h.selector,
                            function: "Ajax.callback.fail()",
                            arguments: JSON.stringify(r.request),
                            message: e
                        }), console.log(i), G.print())
                    }).always(function(t, e, i) {
                        for (var o, s = 0, n = r.validForGroup.length; s < n; s++) {
                            if (o = r.validForGroup[s], (a = h.requests[o]).callback.always instanceof Function && a.callback.always.call(h, t, e, i), "object" != typeof i) return;
                            h.populateSource(null !== t && "function" == typeof t.promise && [] || l[o] || t, a.extra.group, a.extra.path || a.request.path), 0 === (c -= 1) && h.helper.executeCallback.call(h, h.options.callback.onReceiveRequest, [h.node, h.query])
                        }
                    }).then(function(t, e) {
                        for (var i = 0, o = r.validForGroup.length; i < o; i++)(a = h.requests[r.validForGroup[i]]).callback.then instanceof Function && a.callback.then.call(h, t, e)
                    })
                }(t, this.requests[t])
        },
        populateSource: function(i, t, e) {
            var o = this,
                s = this.options.source[t],
                n = s.ajax && s.data;
            e && "string" == typeof e && (i = this.helper.namespace.call(this, e, i)), void 0 === i && this.options.debug && (G.log({
                node: this.selector,
                function: "populateSource()",
                arguments: e,
                message: "Invalid data path."
            }), G.print()), Array.isArray(i) || (this.options.debug && (G.log({
                node: this.selector,
                function: "populateSource()",
                arguments: JSON.stringify({
                    group: t
                }),
                message: "Invalid data type, must be Array type."
            }), G.print()), i = []), n && ("function" == typeof n && (n = n()), Array.isArray(n) ? i = i.concat(n) : this.options.debug && (G.log({
                node: this.selector,
                function: "populateSource()",
                arguments: JSON.stringify(n),
                message: "WARNING - this.options.source." + t + ".data Must be an Array or a function that returns an Array."
            }), G.print()));
            for (var r, a = s.display ? "compiled" === s.display[0] ? s.display[1] : s.display[0] : "compiled" === this.options.display[0] ? this.options.display[1] : this.options.display[0], l = 0, h = i.length; l < h; l++) null !== i[l] && "boolean" != typeof i[l] ? ("string" == typeof i[l] && ((r = {})[a] = i[l], i[l] = r), i[l].group = t) : this.options.debug && (G.log({
                node: this.selector,
                function: "populateSource()",
                message: "WARNING - NULL/BOOLEAN value inside " + t + "! The data was skipped."
            }), G.print());
            if (!this.hasDynamicGroups && this.dropdownFilter.dynamic.length) {
                var c, u, p = {};
                for (l = 0, h = i.length; l < h; l++)
                    for (var d = 0, f = this.dropdownFilter.dynamic.length; d < f; d++) c = this.dropdownFilter.dynamic[d].key, (u = i[l][c]) && (this.dropdownFilter.dynamic[d].value || (this.dropdownFilter.dynamic[d].value = []), p[c] || (p[c] = []), ~p[c].indexOf(u.toLowerCase()) || (p[c].push(u.toLowerCase()), this.dropdownFilter.dynamic[d].value.push(u)))
            }
            if (this.options.correlativeTemplate) {
                var g = s.template || this.options.template,
                    m = "";
                if ("function" == typeof g && (g = g.call(this, "", {})), g) {
                    if (Array.isArray(this.options.correlativeTemplate))
                        for (l = 0, h = this.options.correlativeTemplate.length; l < h; l++) m += "{{" + this.options.correlativeTemplate[l] + "}} ";
                    else m = g.replace(/<.+?>/g, " ").replace(/\s{2,}/, " ").trim();
                    for (l = 0, h = i.length; l < h; l++) i[l].compiled = T("<textarea />").html(m.replace(/\{\{([\w\-\.]+)(?:\|(\w+))?}}/g, function(t, e) {
                        return o.helper.namespace.call(o, e, i[l], "get", "")
                    }).trim()).text();
                    s.display ? ~s.display.indexOf("compiled") || s.display.unshift("compiled") : ~this.options.display.indexOf("compiled") || this.options.display.unshift("compiled")
                } else this.options.debug && (G.log({
                    node: this.selector,
                    function: "populateSource()",
                    arguments: String(t),
                    message: "WARNING - this.options.correlativeTemplate is enabled but no template was found."
                }), G.print())
            }
            this.options.callback.onPopulateSource && (i = this.helper.executeCallback.call(this, this.options.callback.onPopulateSource, [this.node, i, t, e]), this.options.debug && (i && Array.isArray(i) || (G.log({
                node: this.selector,
                function: "callback.populateSource()",
                message: 'callback.onPopulateSource must return the "data" parameter'
            }), G.print()))), this.tmpSource[t] = Array.isArray(i) && i || [];
            var y = this.options.source[t].cache,
                b = this.options.source[t].compression,
                v = this.options.source[t].ttl || this.options.ttl;
            if (y && !window[y].getItem("TYPEAHEAD_" + this.selector + ":" + t)) {
                this.options.callback.onCacheSave && (i = this.helper.executeCallback.call(this, this.options.callback.onCacheSave, [this.node, i, t, e]), this.options.debug && (i && Array.isArray(i) || (G.log({
                    node: this.selector,
                    function: "callback.populateSource()",
                    message: 'callback.onCacheSave must return the "data" parameter'
                }), G.print())));
                var k = JSON.stringify({
                    data: i,
                    ttl: (new Date).getTime() + v
                });
                b && (k = LZString.compressToUTF16(k)), window[y].setItem("TYPEAHEAD_" + this.selector + ":" + t, k)
            }
            this.incrementGeneratedGroup()
        },
        incrementGeneratedGroup: function() {
            if (this.generatedGroupCount++, this.generatedGroupCount === this.generateGroups.length) {
                this.xhr = {};
                for (var t = 0, e = this.generateGroups.length; t < e; t++) this.source[this.generateGroups[t]] = this.tmpSource[this.generateGroups[t]];
                this.hasDynamicGroups || this.buildDropdownItemLayout("dynamic"), this.options.loadingAnimation && this.container.removeClass("loading"), this.node.trigger("search" + this.namespace)
            }
        },
        navigate: function(t) {
            if (this.helper.executeCallback.call(this, this.options.callback.onNavigateBefore, [this.node, this.query, t]), 27 === t.keyCode) return t.preventDefault(), void(this.query.length ? (this.resetInput(), this.node.trigger("input" + this.namespace, [t])) : (this.node.blur(), this.hideLayout()));
            if (this.result.length) {
                var e, i = this.resultContainer.find("." + this.options.selector.item).not("[disabled]"),
                    o = i.filter(".active"),
                    s = o[0] ? i.index(o) : null,
                    n = o[0] ? o.attr("data-index") : null,
                    r = null;
                if (this.clearActiveItem(), this.helper.executeCallback.call(this, this.options.callback.onLeave, [this.node, null !== s && i.eq(s) || void 0, null !== n && this.result[n] || void 0, t]), 13 === t.keyCode) return t.preventDefault(), void(0 < o.length ? "javascript:;" === o.find("a:first")[0].href ? o.find("a:first").trigger("click", t) : o.find("a:first")[0].click() : this.node.closest("form").trigger("submit"));
                if (39 !== t.keyCode) {
                    9 === t.keyCode ? this.options.blurOnTab ? this.hideLayout() : 0 < o.length ? s + 1 < i.length ? (t.preventDefault(), r = s + 1, this.addActiveItem(i.eq(r))) : this.hideLayout() : i.length ? (t.preventDefault(), r = 0, this.addActiveItem(i.first())) : this.hideLayout() : 38 === t.keyCode ? (t.preventDefault(), 0 < o.length ? 0 <= s - 1 && (r = s - 1, this.addActiveItem(i.eq(r))) : i.length && (r = i.length - 1, this.addActiveItem(i.last()))) : 40 === t.keyCode && (t.preventDefault(), 0 < o.length ? s + 1 < i.length && (r = s + 1, this.addActiveItem(i.eq(r))) : i.length && (r = 0, this.addActiveItem(i.first()))), e = null !== r ? i.eq(r).attr("data-index") : null, this.helper.executeCallback.call(this, this.options.callback.onEnter, [this.node, null !== r && i.eq(r) || void 0, null !== e && this.result[e] || void 0, t]), t.preventInputChange && ~[38, 40].indexOf(t.keyCode) && this.buildHintLayout(null !== e && e < this.result.length ? [this.result[e]] : null), this.options.hint && this.hint.container && this.hint.container.css("color", t.preventInputChange ? this.hint.css.color : null === e && this.hint.css.color || this.hint.container.css("background-color") || "fff");
                    var a = null === e || t.preventInputChange ? this.rawQuery : this.getTemplateValue.call(this, this.result[e]);
                    this.node.val(a), this.isContentEditable && this.node.text(a), this.helper.executeCallback.call(this, this.options.callback.onNavigateAfter, [this.node, i, null !== r && i.eq(r).find("a:first") || void 0, null !== e && this.result[e] || void 0, this.query, t])
                } else null !== s ? i.eq(s).find("a:first")[0].click() : this.options.hint && "" !== this.hint.container.val() && this.helper.getCaret(this.node[0]) >= this.query.length && i.filter('[data-index="' + this.hintIndex + '"]').find("a:first")[0].click()
            }
        },
        getTemplateValue: function(i) {
            if (i) {
                var t = i.group && this.options.source[i.group].templateValue || this.options.templateValue;
                if ("function" == typeof t && (t = t.call(this)), !t) return this.helper.namespace.call(this, i.matchedKey, i).toString();
                var o = this;
                return t.replace(/\{\{([\w\-.]+)}}/gi, function(t, e) {
                    return o.helper.namespace.call(o, e, i, "get", "")
                })
            }
        },
        clearActiveItem: function() {
            this.resultContainer.find("." + this.options.selector.item).removeClass("active")
        },
        addActiveItem: function(t) {
            t.addClass("active")
        },
        searchResult: function() {
            this.resetLayout(), !1 !== this.helper.executeCallback.call(this, this.options.callback.onSearch, [this.node, this.query]) && (!this.searchGroups.length || this.options.multiselect && this.options.multiselect.limit && this.items.length >= this.options.multiselect.limit || this.searchResultData(), this.helper.executeCallback.call(this, this.options.callback.onResult, [this.node, this.query, this.result, this.resultCount, this.resultCountPerGroup]), this.isDropdownEvent && (this.helper.executeCallback.call(this, this.options.callback.onDropdownFilter, [this.node, this.query, this.filters.dropdown, this.result]), this.isDropdownEvent = !1))
        },
        searchResultData: function() {
            var t, e, i, o, s, n, r, a, l, h, c, u = this.groupBy,
                p = null,
                d = this.query.toLowerCase(),
                f = this.options.maxItem,
                g = this.options.maxItemPerGroup,
                m = this.filters.dynamic && !this.helper.isEmpty(this.filters.dynamic),
                y = {},
                b = "function" == typeof this.options.matcher && this.options.matcher;
            this.options.accent && (d = this.helper.removeAccent.call(this, d));
            for (var v = 0, k = this.searchGroups.length; v < k; ++v)
                if (j = this.searchGroups[v], !this.filters.dropdown || "group" !== this.filters.dropdown.key || this.filters.dropdown.value === j) {
                    s = void 0 !== this.options.source[j].filter ? this.options.source[j].filter : this.options.filter, r = "function" == typeof this.options.source[j].matcher && this.options.source[j].matcher || b;
                    for (var w = 0, x = this.source[j].length; w < x && (!(this.resultItemCount >= f) || this.options.callback.onResult); w++)
                        if ((!m || this.dynamicFilter.validate.apply(this, [this.source[j][w]])) && null !== (t = this.source[j][w]) && "boolean" != typeof t && (!this.options.multiselect || this.isMultiselectUniqueData(t)) && (!this.filters.dropdown || (t[this.filters.dropdown.key] || "").toLowerCase() === (this.filters.dropdown.value || "").toLowerCase())) {
                            if ((p = "group" === u ? j : t[u] ? t[u] : t.group) && !this.tmpResult[p] && (this.tmpResult[p] = [], this.resultCountPerGroup[p] = 0), g && "group" === u && this.tmpResult[p].length >= g && !this.options.callback.onResult) break;
                            for (var C = 0, q = (L = this.options.source[j].display || this.options.display).length; C < q; ++C) {
                                if (!1 !== s) {
                                    if (void 0 === (o = /\./.test(L[C]) ? this.helper.namespace.call(this, L[C], t) : t[L[C]]) || "" === o) {
                                        this.options.debug && (y[C] = {
                                            display: L[C],
                                            data: t
                                        });
                                        continue
                                    }
                                    o = this.helper.cleanStringFromScript(o)
                                }
                                if ("function" == typeof s) {
                                    if (void 0 === (n = s.call(this, t, o))) break;
                                    if (!n) continue;
                                    "object" == typeof n && (t = n)
                                }
                                if (~[void 0, !0].indexOf(s)) {
                                    if (i = (i = o).toString().toLowerCase(), this.options.accent && (i = this.helper.removeAccent.call(this, i)), e = i.indexOf(d), this.options.correlativeTemplate && "compiled" === L[C] && e < 0 && /\s/.test(d)) {
                                        l = !0, c = i;
                                        for (var S = 0, A = (h = d.split(" ")).length; S < A; S++)
                                            if ("" !== h[S]) {
                                                if (!~c.indexOf(h[S])) {
                                                    l = !1;
                                                    break
                                                }
                                                c = c.replace(h[S], "")
                                            }
                                    }
                                    if (e < 0 && !l) continue;
                                    if (this.options.offset && 0 !== e) continue;
                                    if (r) {
                                        if (void 0 === (a = r.call(this, t, o))) break;
                                        if (!a) continue;
                                        "object" == typeof a && (t = a)
                                    }
                                }
                                if (this.resultCount++, this.resultCountPerGroup[p]++, this.resultItemCount < f) {
                                    if (g && this.tmpResult[p].length >= g) break;
                                    this.tmpResult[p].push(T.extend(!0, {
                                        matchedKey: L[C]
                                    }, t)), this.resultItemCount++
                                }
                                break
                            }
                            if (!this.options.callback.onResult) {
                                if (this.resultItemCount >= f) break;
                                if (g && this.tmpResult[p].length >= g && "group" === u) break
                            }
                        }
                }
            if (this.options.debug && (this.helper.isEmpty(y) || (G.log({
                    node: this.selector,
                    function: "searchResult()",
                    arguments: JSON.stringify(y),
                    message: "Missing keys for display, make sure options.display is set properly."
                }), G.print())), this.options.order) {
                var O, L = [];
                for (var j in this.tmpResult)
                    if (this.tmpResult.hasOwnProperty(j)) {
                        for (v = 0, k = this.tmpResult[j].length; v < k; v++) O = this.options.source[this.tmpResult[j][v].group].display || this.options.display, ~L.indexOf(O[0]) || L.push(O[0]);
                        this.tmpResult[j].sort(this.helper.sort(L, "asc" === this.options.order, function(t) {
                            return t.toString().toUpperCase()
                        }))
                    }
            }
            var F = [],
                I = [];
            for (v = 0, k = (I = "function" == typeof this.options.groupOrder ? this.options.groupOrder.apply(this, [this.node, this.query, this.tmpResult, this.resultCount, this.resultCountPerGroup]) : Array.isArray(this.options.groupOrder) ? this.options.groupOrder : "string" == typeof this.options.groupOrder && ~["asc", "desc"].indexOf(this.options.groupOrder) ? Object.keys(this.tmpResult).sort(this.helper.sort([], "asc" === this.options.groupOrder, function(t) {
                    return t.toString().toUpperCase()
                })) : Object.keys(this.tmpResult)).length; v < k; v++) F = F.concat(this.tmpResult[I[v]] || []);
            this.groups = JSON.parse(JSON.stringify(I)), this.result = F
        },
        buildLayout: function() {
            this.buildHtmlLayout(), this.buildBackdropLayout(), this.buildHintLayout(), this.options.callback.onLayoutBuiltBefore && this.helper.executeCallback.call(this, this.options.callback.onLayoutBuiltBefore, [this.node, this.query, this.result, this.resultHtml]), this.resultHtml instanceof T && this.resultContainer.html(this.resultHtml), this.options.callback.onLayoutBuiltAfter && this.helper.executeCallback.call(this, this.options.callback.onLayoutBuiltAfter, [this.node, this.query, this.result])
        },
        buildHtmlLayout: function() {
            if (!1 !== this.options.resultContainer) {
                var h;
                if (this.resultContainer || (this.resultContainer = T("<div/>", {
                        class: this.options.selector.result
                    }), this.container.append(this.resultContainer)), !this.result.length)
                    if (this.options.multiselect && this.options.multiselect.limit && this.items.length >= this.options.multiselect.limit) h = this.options.multiselect.limitTemplate ? "function" == typeof this.options.multiselect.limitTemplate ? this.options.multiselect.limitTemplate.call(this, this.query) : this.options.multiselect.limitTemplate.replace(/\{\{query}}/gi, T("<div>").text(this.helper.cleanStringFromScript(this.query)).html()) : "Can't select more than " + this.items.length + " items.";
                    else {
                        if (!this.options.emptyTemplate || "" === this.query) return;
                        h = "function" == typeof this.options.emptyTemplate ? this.options.emptyTemplate.call(this, this.query) : this.options.emptyTemplate.replace(/\{\{query}}/gi, T("<div>").text(this.helper.cleanStringFromScript(this.query)).html())
                    }
                var s = this.query.toLowerCase();
                this.options.accent && (s = this.helper.removeAccent.call(this, s));
                var c = this,
                    t = this.groupTemplate || "<ul></ul>",
                    u = !1;
                this.groupTemplate ? t = T(t.replace(/<([^>]+)>\{\{(.+?)}}<\/[^>]+>/g, function(t, e, i, o, s) {
                    var n = "",
                        r = "group" === i ? c.groups : [i];
                    if (!c.result.length) return !0 === u ? "" : (u = !0, "<" + e + ' class="' + c.options.selector.empty + '">' + h + "</" + e + ">");
                    for (var a = 0, l = r.length; a < l; ++a) n += "<" + e + ' data-group-template="' + r[a] + '"><ul></ul></' + e + ">";
                    return n
                })) : (t = T(t), this.result.length || t.append(h instanceof T ? h : '<li class="' + c.options.selector.empty + '">' + h + "</li>")), t.addClass(this.options.selector.list + (this.helper.isEmpty(this.result) ? " empty" : ""));
                for (var e, i, n, o, r, a, l, p, d, f, g, m, y, b = this.groupTemplate && this.result.length && c.groups || [], v = 0, k = this.result.length; v < k; ++v) e = (n = this.result[v]).group, o = !this.options.multiselect && this.options.source[n.group].href || this.options.href, p = [], d = this.options.source[n.group].display || this.options.display, this.options.group && (e = n[this.options.group.key], this.options.group.template && ("function" == typeof this.options.group.template ? i = this.options.group.template.call(this, n) : "string" == typeof this.options.group.template && (i = this.options.group.template.replace(/\{\{([\w\-\.]+)}}/gi, function(t, e) {
                        return c.helper.namespace.call(c, e, n, "get", "")
                    }))), t.find('[data-search-group="' + e + '"]')[0] || (this.groupTemplate ? t.find('[data-group-template="' + e + '"] ul') : t).append(T("<li/>", {
                        class: c.options.selector.group,
                        html: T("<a/>", {
                            href: "javascript:;",
                            html: i || e,
                            tabindex: -1
                        }),
                        "data-search-group": e
                    }))), this.groupTemplate && b.length && ~(g = b.indexOf(e || n.group)) && b.splice(g, 1), r = T("<li/>", {
                        class: c.options.selector.item + " " + c.options.selector.group + "-" + this.helper.slugify.call(this, e),
                        disabled: !!n.disabled,
                        "data-group": e,
                        "data-index": v,
                        html: T("<a/>", {
                            href: o && !n.disabled ? (m = o, y = n, y.href = c.generateHref.call(c, m, y)) : "javascript:;",
                            html: function() {
                                if (a = n.group && c.options.source[n.group].template || c.options.template) "function" == typeof a && (a = a.call(c, c.query, n)), l = a.replace(/\{\{([^\|}]+)(?:\|([^}]+))*}}/gi, function(t, e, i) {
                                    var o = c.helper.cleanStringFromScript(String(c.helper.namespace.call(c, e, n, "get", "")));
                                    return ~(i = i && i.split("|") || []).indexOf("slugify") && (o = c.helper.slugify.call(c, o)), ~i.indexOf("raw") || !0 === c.options.highlight && s && ~d.indexOf(e) && (o = c.helper.highlight.call(c, o, s.split(" "), c.options.accent)), o
                                });
                                else {
                                    for (var t = 0, e = d.length; t < e; t++) void 0 !== (f = /\./.test(d[t]) ? c.helper.namespace.call(c, d[t], n, "get", "") : n[d[t]]) && "" !== f && p.push(f);
                                    l = '<span class="' + c.options.selector.display + '">' + c.helper.cleanStringFromScript(String(p.join(" "))) + "</span>"
                                }(!0 === c.options.highlight && s && !a || "any" === c.options.highlight) && (l = c.helper.highlight.call(c, l, s.split(" "), c.options.accent)), T(this).append(l)
                            }
                        })
                    }),
                    function(t, o, e) {
                        e.on("click", function(t, e) {
                            if (o.disabled) t.preventDefault();
                            else if (e && "object" == typeof e && (t.originalEvent = e), c.options.mustSelectItem && c.helper.isEmpty(o)) t.preventDefault();
                            else if (c.options.multiselect ? (c.items.push(o), c.comparedItems.push(c.getMultiselectComparedData(o))) : c.item = o, !1 !== c.helper.executeCallback.call(c, c.options.callback.onClickBefore, [c.node, T(this), o, t]) && !(t.originalEvent && t.originalEvent.defaultPrevented || t.isDefaultPrevented())) {
                                var i = c.getTemplateValue.call(c, o);
                                c.options.multiselect ? (c.query = c.rawQuery = "", c.addMultiselectItemLayout(i)) : (c.focusOnly = !0, c.query = c.rawQuery = i, c.isContentEditable && (c.node.text(c.query), c.helper.setCaretAtEnd(c.node[0]))), c.hideLayout(), c.node.val(c.query).focus(), c.helper.executeCallback.call(c, c.options.callback.onClickAfter, [c.node, T(this), o, t])
                            }
                        }), e.on("mouseenter", function(t) {
                            o.disabled || (c.clearActiveItem(), c.addActiveItem(T(this))), c.helper.executeCallback.call(c, c.options.callback.onEnter, [c.node, T(this), o, t])
                        }), e.on("mouseleave", function(t) {
                            o.disabled || c.clearActiveItem(), c.helper.executeCallback.call(c, c.options.callback.onLeave, [c.node, T(this), o, t])
                        })
                    }(0, n, r), (this.groupTemplate ? t.find('[data-group-template="' + e + '"] ul') : t).append(r);
                if (this.result.length && b.length)
                    for (v = 0, k = b.length; v < k; ++v) t.find('[data-group-template="' + b[v] + '"]').remove();
                this.resultHtml = t
            }
        },
        generateHref: function(t, s) {
            var n = this;
            return "string" == typeof t ? t = t.replace(/\{\{([^\|}]+)(?:\|([^}]+))*}}/gi, function(t, e, i) {
                var o = n.helper.namespace.call(n, e, s, "get", "");
                return ~(i = i && i.split("|") || []).indexOf("slugify") && (o = n.helper.slugify.call(n, o)), o
            }) : "function" == typeof t && (t = t.call(this, s)), t
        },
        getMultiselectComparedData: function(t) {
            var e = "";
            if (Array.isArray(this.options.multiselect.matchOn))
                for (var i = 0, o = this.options.multiselect.matchOn.length; i < o; ++i) e += void 0 !== t[this.options.multiselect.matchOn[i]] ? t[this.options.multiselect.matchOn[i]] : "";
            else {
                var s = JSON.parse(JSON.stringify(t)),
                    n = ["group", "matchedKey", "compiled", "href"];
                for (i = 0, o = n.length; i < o; ++i) delete s[n[i]];
                e = JSON.stringify(s)
            }
            return e
        },
        buildBackdropLayout: function() {
            this.options.backdrop && (this.backdrop.container || (this.backdrop.css = T.extend({
                opacity: .6,
                filter: "alpha(opacity=60)",
                position: "fixed",
                top: 0,
                right: 0,
                bottom: 0,
                left: 0,
                "z-index": 1040,
                "background-color": "#000"
            }, this.options.backdrop), this.backdrop.container = T("<div/>", {
                class: this.options.selector.backdrop,
                css: this.backdrop.css
            }).insertAfter(this.container)), this.container.addClass("backdrop").css({
                "z-index": this.backdrop.css["z-index"] + 1,
                position: "relative"
            }))
        },
        buildHintLayout: function(t) {
            if (this.options.hint)
                if (this.node[0].scrollWidth > Math.ceil(this.node.innerWidth())) this.hint.container && this.hint.container.val("");
                else {
                    var e = this,
                        i = "",
                        o = (t = t || this.result, this.query.toLowerCase());
                    if (this.options.accent && (o = this.helper.removeAccent.call(this, o)), this.hintIndex = null, this.searchGroups.length) {
                        if (this.hint.container || (this.hint.css = T.extend({
                                "border-color": "transparent",
                                position: "absolute",
                                top: 0,
                                display: "inline",
                                "z-index": -1,
                                float: "none",
                                color: "silver",
                                "box-shadow": "none",
                                cursor: "default",
                                "-webkit-user-select": "none",
                                "-moz-user-select": "none",
                                "-ms-user-select": "none",
                                "user-select": "none"
                            }, this.options.hint), this.hint.container = T("<" + this.node[0].nodeName + "/>", {
                                type: this.node.attr("type"),
                                class: this.node.attr("class"),
                                readonly: !0,
                                unselectable: "on",
                                "aria-hidden": "true",
                                tabindex: -1,
                                click: function() {
                                    e.node.focus()
                                }
                            }).addClass(this.options.selector.hint).css(this.hint.css).insertAfter(this.node), this.node.parent().css({
                                position: "relative"
                            })), this.hint.container.css("color", this.hint.css.color), o)
                            for (var s, n, r, a = 0, l = t.length; a < l; a++)
                                if (!t[a].disabled) {
                                    n = t[a].group;
                                    for (var h = 0, c = (s = this.options.source[n].display || this.options.display).length; h < c; h++)
                                        if (r = String(t[a][s[h]]).toLowerCase(), this.options.accent && (r = this.helper.removeAccent.call(this, r)), 0 === r.indexOf(o)) {
                                            i = String(t[a][s[h]]), this.hintIndex = a;
                                            break
                                        }
                                    if (null !== this.hintIndex) break
                                }
                        var u = 0 < i.length && this.rawQuery + i.substring(this.query.length) || "";
                        this.hint.container.val(u), this.isContentEditable && this.hint.container.text(u)
                    }
                }
        },
        buildDropdownLayout: function() {
            if (this.options.dropdownFilter) {
                var i = this;
                T("<span/>", {
                    class: this.options.selector.filter,
                    html: function() {
                        T(this).append(T("<button/>", {
                            type: "button",
                            class: i.options.selector.filterButton,
                            style: "display: none;",
                            click: function() {
                                i.container.toggleClass("filter");
                                var e = i.namespace + "-dropdown-filter";
                                T("html").off(e), i.container.hasClass("filter") && T("html").on("click" + e + " touchend" + e, function(t) {
                                    T(t.target).closest("." + i.options.selector.filter)[0] && T(t.target).closest(i.container)[0] || i.hasDragged || (i.container.removeClass("filter"), T("html").off(e))
                                })
                            }
                        })), T(this).append(T("<ul/>", {
                            class: i.options.selector.dropdown
                        }))
                    }
                }).insertAfter(i.container.find("." + i.options.selector.query))
            }
        },
        buildDropdownItemLayout: function(t) {
            function s(t) {
                "*" === t.value ? delete this.filters.dropdown : this.filters.dropdown = t, this.container.removeClass("filter").find("." + this.options.selector.filterButton).html(t.template), this.isDropdownEvent = !0, this.node.trigger("search" + this.namespace), this.options.multiselect && this.adjustInputSize(), this.node.focus()
            }
            if (this.options.dropdownFilter) {
                var e, i, n = this,
                    r = "string" == typeof this.options.dropdownFilter && this.options.dropdownFilter || "All",
                    a = this.container.find("." + this.options.selector.dropdown);
                "static" !== t || !0 !== this.options.dropdownFilter && "string" != typeof this.options.dropdownFilter || this.dropdownFilter.static.push({
                    key: "group",
                    template: "{{group}}",
                    all: r,
                    value: Object.keys(this.options.source)
                });
                for (var o = 0, l = this.dropdownFilter[t].length; o < l; o++) {
                    i = this.dropdownFilter[t][o], Array.isArray(i.value) || (i.value = [i.value]), i.all && (this.dropdownFilterAll = i.all);
                    for (var h = 0, c = i.value.length; h <= c; h++) h === c && o !== l - 1 || h === c && o === l - 1 && "static" === t && this.dropdownFilter.dynamic.length || (e = this.dropdownFilterAll || r, i.value[h] ? e = i.template ? i.template.replace(new RegExp("{{" + i.key + "}}", "gi"), i.value[h]) : i.value[h] : this.container.find("." + n.options.selector.filterButton).html(e), function(e, i, o) {
                        a.append(T("<li/>", {
                            class: n.options.selector.dropdownItem + " " + n.helper.slugify.call(n, i.key + "-" + (i.value[e] || r)),
                            html: T("<a/>", {
                                href: "javascript:;",
                                html: o,
                                click: function(t) {
                                    t.preventDefault(), s.call(n, {
                                        key: i.key,
                                        value: i.value[e] || "*",
                                        template: o
                                    })
                                }
                            })
                        }))
                    }(h, i, e))
                }
                this.dropdownFilter[t].length && this.container.find("." + n.options.selector.filterButton).removeAttr("style")
            }
        },
        dynamicFilter: {
            isEnabled: !1,
            init: function() {
                this.options.dynamicFilter && (this.dynamicFilter.bind.call(this), this.dynamicFilter.isEnabled = !0)
            },
            validate: function(t) {
                var e, i, o = null,
                    s = null;
                for (var n in this.filters.dynamic)
                    if (this.filters.dynamic.hasOwnProperty(n) && (i = ~n.indexOf(".") ? this.helper.namespace.call(this, n, t, "get") : t[n], "|" !== this.filters.dynamic[n].modifier || o || (o = i == this.filters.dynamic[n].value || !1), "&" === this.filters.dynamic[n].modifier)) {
                        if (i != this.filters.dynamic[n].value) {
                            s = !1;
                            break
                        }
                        s = !0
                    }
                return e = o, null !== s && !0 === (e = s) && null !== o && (e = o), !!e
            },
            set: function(t, e) {
                var i = t.match(/^([|&])?(.+)/);
                e ? this.filters.dynamic[i[2]] = {
                    modifier: i[1] || "|",
                    value: e
                } : delete this.filters.dynamic[i[2]], this.dynamicFilter.isEnabled && this.generateSource()
            },
            bind: function() {
                for (var t, e = this, i = 0, o = this.options.dynamicFilter.length; i < o; i++) "string" == typeof(t = this.options.dynamicFilter[i]).selector && (t.selector = T(t.selector)), t.selector instanceof T && t.selector[0] && t.key ? function(t) {
                    t.selector.off(e.namespace).on("change" + e.namespace, function() {
                        e.dynamicFilter.set.apply(e, [t.key, e.dynamicFilter.getValue(this)])
                    }).trigger("change" + e.namespace)
                }(t) : this.options.debug && (G.log({
                    node: this.selector,
                    function: "buildDynamicLayout()",
                    message: 'Invalid jQuery selector or jQuery Object for "filter.selector" or missing filter.key'
                }), G.print())
            },
            getValue: function(t) {
                var e;
                return "SELECT" === t.tagName ? e = t.value : "INPUT" === t.tagName && ("checkbox" === t.type ? e = t.checked && t.getAttribute("value") || t.checked || null : "radio" === t.type && t.checked && (e = t.value)), e
            }
        },
        buildMultiselectLayout: function() {
            if (this.options.multiselect) {
                var t, e = this;
                this.label.container = T("<span/>", {
                    class: this.options.selector.labelContainer,
                    "data-padding-left": parseFloat(this.node.css("padding-left")) || 0,
                    "data-padding-right": parseFloat(this.node.css("padding-right")) || 0,
                    "data-padding-top": parseFloat(this.node.css("padding-top")) || 0,
                    click: function(t) {
                        T(t.target).hasClass(e.options.selector.labelContainer) && e.node.focus()
                    }
                }), this.node.closest("." + this.options.selector.query).prepend(this.label.container), this.options.multiselect.data && (Array.isArray(this.options.multiselect.data) ? this.populateMultiselectData(this.options.multiselect.data) : "function" == typeof this.options.multiselect.data && (t = this.options.multiselect.data.call(this), Array.isArray(t) ? this.populateMultiselectData(t) : "function" == typeof t.promise && T.when(t).then(function(t) {
                    t && Array.isArray(t) && e.populateMultiselectData(t)
                })))
            }
        },
        isMultiselectUniqueData: function(t) {
            for (var e = !0, i = 0, o = this.comparedItems.length; i < o; ++i)
                if (this.comparedItems[i] === this.getMultiselectComparedData(t)) {
                    e = !1;
                    break
                }
            return e
        },
        populateMultiselectData: function(t) {
            for (var e = 0, i = t.length; e < i; ++e) this.isMultiselectUniqueData(t[e]) && (this.items.push(t[e]), this.comparedItems.push(this.getMultiselectComparedData(t[e])), this.addMultiselectItemLayout(this.getTemplateValue(t[e])));
            this.node.trigger("search" + this.namespace, {
                origin: "populateMultiselectData"
            })
        },
        addMultiselectItemLayout: function(t) {
            var e, o = this,
                i = this.options.multiselect.href ? "a" : "span",
                s = T("<span/>", {
                    class: this.options.selector.label,
                    html: T("<" + i + "/>", {
                        text: t,
                        click: function(t) {
                            var e = T(this).closest("." + o.options.selector.label),
                                i = o.label.container.find("." + o.options.selector.label).index(e);
                            o.options.multiselect.callback && o.helper.executeCallback.call(o, o.options.multiselect.callback.onClick, [o.node, o.items[i], t])
                        },
                        href: this.options.multiselect.href ? (e = o.items[o.items.length - 1], o.generateHref.call(o, o.options.multiselect.href, e)) : null
                    })
                });
            s.append(T("<span/>", {
                class: this.options.selector.cancelButton,
                html: "Ã—",
                click: function(t) {
                    var e = T(this).closest("." + o.options.selector.label),
                        i = o.label.container.find("." + o.options.selector.label).index(e);
                    o.cancelMultiselectItem(i, e, t)
                }
            })), this.label.container.append(s), this.adjustInputSize()
        },
        cancelMultiselectItem: function(t, e, i) {
            var o = this.items[t];
            (e = e || this.label.container.find("." + this.options.selector.label).eq(t)).remove(), this.items.splice(t, 1), this.comparedItems.splice(t, 1), this.options.multiselect.callback && this.helper.executeCallback.call(this, this.options.multiselect.callback.onCancel, [this.node, o, i]), this.adjustInputSize(), this.focusOnly = !0, this.node.focus().trigger("input" + this.namespace, {
                origin: "cancelMultiselectItem"
            })
        },
        adjustInputSize: function() {
            var i = this.node[0].getBoundingClientRect().width - (parseFloat(this.label.container.data("padding-right")) || 0) - (parseFloat(this.label.container.css("padding-left")) || 0),
                o = 0,
                s = 0,
                n = 0,
                r = !1,
                a = 0;
            this.label.container.find("." + this.options.selector.label).filter(function(t, e) {
                0 === t && (a = T(e)[0].getBoundingClientRect().height + parseFloat(T(e).css("margin-bottom") || 0)), o = T(e)[0].getBoundingClientRect().width + parseFloat(T(e).css("margin-right") || 0), .7 * i < n + o && !r && (s++, r = !0), n + o < i ? n += o : (r = !1, n = o)
            });
            var t = parseFloat(this.label.container.data("padding-left") || 0) + (r ? 0 : n),
                e = s * a + parseFloat(this.label.container.data("padding-top") || 0);
            this.container.find("." + this.options.selector.query).find("input, textarea, [contenteditable], .typeahead__hint").css({
                paddingLeft: t,
                paddingTop: e
            })
        },
        showLayout: function() {
            !this.container.hasClass("result") && (this.result.length || this.options.emptyTemplate || this.options.backdropOnFocus) && (function() {
                var e = this;
                T("html").off("keydown" + this.namespace).on("keydown" + this.namespace, function(t) {
                    t.keyCode && 9 === t.keyCode && setTimeout(function() {
                        T(":focus").closest(e.container).find(e.node)[0] || e.hideLayout()
                    }, 0)
                }), T("html").off("click" + this.namespace + " touchend" + this.namespace).on("click" + this.namespace + " touchend" + this.namespace, function(t) {
                    T(t.target).closest(e.container)[0] || T(t.target).closest("." + e.options.selector.item)[0] || t.target.className === e.options.selector.cancelButton || e.hasDragged || e.hideLayout()
                })
            }.call(this), this.container.addClass([this.result.length || this.searchGroups.length && this.options.emptyTemplate && this.query.length ? "result " : "", this.options.hint && this.searchGroups.length ? "hint" : "", this.options.backdrop || this.options.backdropOnFocus ? "backdrop" : ""].join(" ")), this.helper.executeCallback.call(this, this.options.callback.onShowLayout, [this.node, this.query]))
        },
        hideLayout: function() {
            (this.container.hasClass("result") || this.container.hasClass("backdrop")) && (this.container.removeClass("result hint filter" + (this.options.backdropOnFocus && T(this.node).is(":focus") ? "" : " backdrop")), this.options.backdropOnFocus && this.container.hasClass("backdrop") || (T("html").off(this.namespace), this.helper.executeCallback.call(this, this.options.callback.onHideLayout, [this.node, this.query])))
        },
        resetLayout: function() {
            this.result = [], this.tmpResult = {}, this.groups = [], this.resultCount = 0, this.resultCountPerGroup = {}, this.resultItemCount = 0, this.resultHtml = null, this.options.hint && this.hint.container && (this.hint.container.val(""), this.isContentEditable && this.hint.container.text(""))
        },
        resetInput: function() {
            this.node.val(""), this.isContentEditable && this.node.text(""), this.item = null, this.query = "", this.rawQuery = ""
        },
        buildCancelButtonLayout: function() {
            if (this.options.cancelButton) {
                var e = this;
                T("<span/>", {
                    class: this.options.selector.cancelButton,
                    html: "Ã—",
                    mousedown: function(t) {
                        t.stopImmediatePropagation(), t.preventDefault(), e.resetInput(), e.node.trigger("input" + e.namespace, [t])
                    }
                }).insertBefore(this.node)
            }
        },
        toggleCancelButtonVisibility: function() {
            this.container.toggleClass("cancel", !!this.query.length)
        },
        __construct: function() {
            this.extendOptions(), this.unifySourceFormat() && (this.dynamicFilter.init.apply(this), this.init(), this.buildDropdownLayout(), this.buildDropdownItemLayout("static"), this.buildMultiselectLayout(), this.delegateEvents(), this.buildCancelButtonLayout(), this.helper.executeCallback.call(this, this.options.callback.onReady, [this.node]))
        },
        helper: {
            isEmpty: function(t) {
                for (var e in t)
                    if (t.hasOwnProperty(e)) return !1;
                return !0
            },
            removeAccent: function(t) {
                if ("string" == typeof t) {
                    var e = s;
                    return "object" == typeof this.options.accent && (e = this.options.accent), t = t.toLowerCase().replace(new RegExp("[" + e.from + "]", "g"), function(t) {
                        return e.to[e.from.indexOf(t)]
                    })
                }
            },
            slugify: function(t) {
                return "" !== (t = String(t)) && (t = (t = this.helper.removeAccent.call(this, t)).replace(/[^-a-z0-9]+/g, "-").replace(/-+/g, "-").replace(/^-|-$/g, "")), t
            },
            sort: function(o, i, s) {
                var n = function(t) {
                    for (var e = 0, i = o.length; e < i; e++)
                        if (void 0 !== t[o[e]]) return s(t[o[e]]);
                    return t
                };
                return i = [-1, 1][+!!i],
                    function(t, e) {
                        return t = n(t), e = n(e), i * ((e < t) - (t < e))
                    }
            },
            replaceAt: function(t, e, i, o) {
                return t.substring(0, e) + o + t.substring(e + i)
            },
            highlight: function(t, e, i) {
                t = String(t);
                var o = i && this.helper.removeAccent.call(this, t) || t,
                    s = [];
                Array.isArray(e) || (e = [e]), e.sort(function(t, e) {
                    return e.length - t.length
                });
                for (var n = e.length - 1; 0 <= n; n--) "" !== e[n].trim() ? e[n] = e[n].replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&") : e.splice(n, 1);
                o.replace(new RegExp("(?:" + e.join("|") + ")(?!([^<]+)?>)", "gi"), function(t, e, i) {
                    s.push({
                        offset: i,
                        length: t.length
                    })
                });
                for (n = s.length - 1; 0 <= n; n--) t = this.helper.replaceAt(t, s[n].offset, s[n].length, "<strong>" + t.substr(s[n].offset, s[n].length) + "</strong>");
                return t
            },
            getCaret: function(t) {
                var e = 0;
                if (t.selectionStart) return t.selectionStart;
                if (document.selection) {
                    var i = document.selection.createRange();
                    if (null === i) return e;
                    var o = t.createTextRange(),
                        s = o.duplicate();
                    o.moveToBookmark(i.getBookmark()), s.setEndPoint("EndToStart", o), e = s.text.length
                } else if (window.getSelection) {
                    var n = window.getSelection();
                    if (n.rangeCount) {
                        var r = n.getRangeAt(0);
                        r.commonAncestorContainer.parentNode == t && (e = r.endOffset)
                    }
                }
                return e
            },
            setCaretAtEnd: function(t) {
                if (void 0 !== window.getSelection && void 0 !== document.createRange) {
                    var e = document.createRange();
                    e.selectNodeContents(t), e.collapse(!1);
                    var i = window.getSelection();
                    i.removeAllRanges(), i.addRange(e)
                } else if (void 0 !== document.body.createTextRange) {
                    var o = document.body.createTextRange();
                    o.moveToElementText(t), o.collapse(!1), o.select()
                }
            },
            cleanStringFromScript: function(t) {
                return "string" == typeof t && t.replace(/<\/?(?:script|iframe)\b[^>]*>/gm, "") || t
            },
            executeCallback: function(t, e) {
                if (t) {
                    var i;
                    if ("function" == typeof t) i = t;
                    else if (("string" == typeof t || Array.isArray(t)) && ("string" == typeof t && (t = [t, []]), "function" != typeof(i = this.helper.namespace.call(this, t[0], window)))) return void(this.options.debug && (G.log({
                        node: this.selector,
                        function: "executeCallback()",
                        arguments: JSON.stringify(t),
                        message: 'WARNING - Invalid callback function"'
                    }), G.print()));
                    return i.apply(this, (t[1] || []).concat(e || []))
                }
            },
            namespace: function(t, e, i, o) {
                if ("string" != typeof t || "" === t) return this.options.debug && (G.log({
                    node: this.options.input || this.selector,
                    function: "helper.namespace()",
                    arguments: t,
                    message: 'ERROR - Missing string"'
                }), G.print()), !1;
                var s = void 0 !== o ? o : void 0;
                if (!~t.indexOf(".")) return e[t] || s;
                for (var n = t.split("."), r = e || window, a = (i = i || "get", ""), l = 0, h = n.length; l < h; l++) {
                    if (void 0 === r[a = n[l]]) {
                        if (~["get", "delete"].indexOf(i)) return void 0 !== o ? o : void 0;
                        r[a] = {}
                    }
                    if (~["set", "create", "delete"].indexOf(i) && l === h - 1) {
                        if ("set" !== i && "create" !== i) return delete r[a], !0;
                        r[a] = s
                    }
                    r = r[a]
                }
                return r
            },
            typeWatch: (i = 0, function(t, e) {
                clearTimeout(i), i = setTimeout(t, e)
            })
        }
    }, T.fn.typeahead = T.typeahead = function(t) {
        return e.typeahead(this, t)
    };
    var e = {
            typeahead: function(t, e) {
                if (!e || !e.source || "object" != typeof e.source) return G.log({
                    node: t.selector || e && e.input,
                    function: "$.typeahead()",
                    arguments: JSON.stringify(e && e.source || ""),
                    message: 'Undefined "options" or "options.source" or invalid source type - Typeahead dropped'
                }), void G.print();
                if ("function" == typeof t) {
                    if (!e.input) return G.log({
                        node: t.selector,
                        function: "$.typeahead()",
                        message: 'Undefined "options.input" - Typeahead dropped'
                    }), void G.print();
                    t = T(e.input)
                }
                if (void 0 === t[0].value && (t[0].value = t.text()), !t.length) return G.log({
                    node: t.selector,
                    function: "$.typeahead()",
                    arguments: JSON.stringify(e.input),
                    message: "Unable to find jQuery input element - Typeahead dropped"
                }), void G.print();
                if (1 === t.length) return t[0].selector = t.selector || e.input || t[0].nodeName.toLowerCase(), window.Typeahead[t[0].selector] = new l(t, e);
                for (var i, o = {}, s = 0, n = t.length; s < n; ++s) void 0 !== o[i = t[s].nodeName.toLowerCase()] && (i += s), t[s].selector = i, window.Typeahead[i] = o[i] = new l(t.eq(s), e);
                return o
            }
        },
        G = {
            table: {},
            log: function(t) {
                t.message && "string" == typeof t.message && (this.table[t.message] = T.extend({
                    node: "",
                    function: "",
                    arguments: ""
                }, t))
            },
            print: function() {
                !l.prototype.helper.isEmpty(this.table) && console && console.table && (void 0 === console.group && void 0 === console.table || (console.groupCollapsed("--- jQuery Typeahead Debug ---"), console.table(this.table), console.groupEnd()), this.table = {})
            }
        };
    return G.log({
        message: "WARNING - You are using the DEBUG version. Use /dist/jquery.typeahead.min.js in production."
    }), G.print(), window.console = window.console || {
        log: function() {}
    }, Array.isArray || (Array.isArray = function(t) {
        return "[object Array]" === Object.prototype.toString.call(t)
    }), "trim" in String.prototype || (String.prototype.trim = function() {
        return this.replace(/^\s+/, "").replace(/\s+$/, "")
    }), "indexOf" in Array.prototype || (Array.prototype.indexOf = function(t, e) {
        void 0 === e && (e = 0), e < 0 && (e += this.length), e < 0 && (e = 0);
        for (var i = this.length; e < i; e++)
            if (e in this && this[e] === t) return e;
        return -1
    }), Object.keys || (Object.keys = function(t) {
        var e, i = [];
        for (e in t) Object.prototype.hasOwnProperty.call(t, e) && i.push(e);
        return i
    }), l
}),
function(t) {
    if ("object" == typeof exports && "undefined" != typeof module) module.exports = t();
    else if ("function" == typeof define && define.amd) define([], t);
    else {
        ("undefined" != typeof window ? window : "undefined" != typeof global ? global : "undefined" != typeof self ? self : this).enquire = t()
    }
}(function() {
    return function n(r, a, l) {
        function h(i, t) {
            if (!a[i]) {
                if (!r[i]) {
                    var e = "function" == typeof require && require;
                    if (!t && e) return e(i, !0);
                    if (c) return c(i, !0);
                    var o = new Error("Cannot find module '" + i + "'");
                    throw o.code = "MODULE_NOT_FOUND", o
                }
                var s = a[i] = {
                    exports: {}
                };
                r[i][0].call(s.exports, function(t) {
                    var e = r[i][1][t];
                    return h(e || t)
                }, s, s.exports, n, r, a, l)
            }
            return a[i].exports
        }
        for (var c = "function" == typeof require && require, t = 0; t < l.length; t++) h(l[t]);
        return h
    }({
        1: [function(t, e, i) {
            function o(t, e) {
                this.query = t, this.isUnconditional = e, this.handlers = [], this.mql = window.matchMedia(t);
                var i = this;
                this.listener = function(t) {
                    i.mql = t.currentTarget || t, i.assess()
                }, this.mql.addListener(this.listener)
            }
            var s = t(3),
                n = t(4).each;
            o.prototype = {
                constuctor: o,
                addHandler: function(t) {
                    var e = new s(t);
                    this.handlers.push(e), this.matches() && e.on()
                },
                removeHandler: function(i) {
                    var o = this.handlers;
                    n(o, function(t, e) {
                        if (t.equals(i)) return t.destroy(), !o.splice(e, 1)
                    })
                },
                matches: function() {
                    return this.mql.matches || this.isUnconditional
                },
                clear: function() {
                    n(this.handlers, function(t) {
                        t.destroy()
                    }), this.mql.removeListener(this.listener), this.handlers.length = 0
                },
                assess: function() {
                    var e = this.matches() ? "on" : "off";
                    n(this.handlers, function(t) {
                        t[e]()
                    })
                }
            }, e.exports = o
        }, {
            3: 3,
            4: 4
        }],
        2: [function(t, e, i) {
            function o() {
                if (!window.matchMedia) throw new Error("matchMedia not present, legacy browsers require a polyfill");
                this.queries = {}, this.browserIsIncapable = !window.matchMedia("only all").matches
            }
            var n = t(1),
                s = t(4),
                r = s.each,
                a = s.isFunction,
                l = s.isArray;
            o.prototype = {
                constructor: o,
                register: function(e, t, i) {
                    var o = this.queries,
                        s = i && this.browserIsIncapable;
                    return o[e] || (o[e] = new n(e, s)), a(t) && (t = {
                        match: t
                    }), l(t) || (t = [t]), r(t, function(t) {
                        a(t) && (t = {
                            match: t
                        }), o[e].addHandler(t)
                    }), this
                },
                unregister: function(t, e) {
                    var i = this.queries[t];
                    return i && (e ? i.removeHandler(e) : (i.clear(), delete this.queries[t])), this
                }
            }, e.exports = o
        }, {
            1: 1,
            4: 4
        }],
        3: [function(t, e, i) {
            function o(t) {
                !(this.options = t).deferSetup && this.setup()
            }
            o.prototype = {
                constructor: o,
                setup: function() {
                    this.options.setup && this.options.setup(), this.initialised = !0
                },
                on: function() {
                    !this.initialised && this.setup(), this.options.match && this.options.match()
                },
                off: function() {
                    this.options.unmatch && this.options.unmatch()
                },
                destroy: function() {
                    this.options.destroy ? this.options.destroy() : this.off()
                },
                equals: function(t) {
                    return this.options === t || this.options.match === t
                }
            }, e.exports = o
        }, {}],
        4: [function(t, e, i) {
            e.exports = {
                isFunction: function(t) {
                    return "function" == typeof t
                },
                isArray: function(t) {
                    return "[object Array]" === Object.prototype.toString.apply(t)
                },
                each: function(t, e) {
                    for (var i = 0, o = t.length; i < o && !1 !== e(t[i], i); i++);
                }
            }
        }, {}],
        5: [function(t, e, i) {
            var o = t(2);
            e.exports = new o
        }, {
            2: 2
        }]
    }, {}, [5])(5)
});