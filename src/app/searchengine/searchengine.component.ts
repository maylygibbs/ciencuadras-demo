import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InmuebleService } from '../services/inmueble/inmueble.service';

@Component({
  selector: 'app-searchengine',
  templateUrl: './searchengine.component.html',
  styleUrls: ['./searchengine.component.scss']
})
export class SearchengineComponent implements OnInit {

  resp: any;
  constructor(private router: Router,  private inmuebleService: InmuebleService) { }

  ngOnInit() {

    this.inmuebleService.redirectLink('josedelgado@gmail.com', 'Jose*delgado01').subscribe(
      resp => {
        this.resp = resp;
        console.log(resp);
      },
      error => {
          console.log(error);
      });
  }

  load_inmuebles_arriendo(tipoTransaccion, tipoInmueble) {
    this.router.navigate([tipoTransaccion, tipoInmueble]);
  }

  load_inmuebles_venta(tipoTransaccion, tipoInmueble) {
    this.router.navigate([tipoTransaccion, tipoInmueble]);
  }

  load_inmuebles_arriendo_bogota(tipoTransaccion, ciudad, tipoInmueble) {
    this.router.navigate([tipoTransaccion, ciudad, tipoInmueble]);
  }

}
