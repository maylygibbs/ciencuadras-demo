import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchengineRoutingModule } from './searchengine-routing.module';
import { SearchengineComponent } from './searchengine.component';
import { InmuebleService } from '../services/inmueble/inmueble.service';

@NgModule({
  declarations: [SearchengineComponent],
  imports: [
    CommonModule,
    SearchengineRoutingModule
  ],
  providers: [InmuebleService]
})
export class SearchengineModule { }
