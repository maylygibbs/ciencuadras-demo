import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';

@Injectable()
export class InmuebleService {


  constructor(private api: ApiService) { }

  getInmuebles(tipoTransaccion, ciudad, tipoInmueble) {
    return this.api.get('inmuebles');
  }

  getInmueblesTemp() {
    return this.api.getFromJsonFile();
  }

  redirectLink(email: string, password: string) {

    return this. api.redirectTo('http://ciencuadras-nuevo.miemprendimientodigital.com/site/login' , email, password);

  }

}
