export class Environments {

    public static islocal = false;

    public static local = 'http://localhost:3000';
	public static production =  'http://localhost:3000';
    public static ENDPOINT = Environments.islocal ? Environments.local : Environments.production;
    public static API_ENDPOINT = `${Environments.ENDPOINT}`;

}
