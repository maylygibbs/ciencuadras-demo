# Ciencuadras Demo

Este proyecto fué generado bajo:
window 10
[Angular CLI](https://github.com/angular/angular-cli) version 7.1.2.
[node] v10.14.1

## Install

Clonar proy -> git clone https://maylygibbs@bitbucket.org/maylygibbs/ciencuadras-demo.git
Cambiar a directorio -> cd ciencuadras-demo 
Ejecutar -> npm insta

## Development server

Ejecuta `ng serve` para habilitar el servidor de desarrollo. 
Navega a `http://localhost:4200/`.

## Production server

Ejecuta `npm run build:ssr && npm run serve:ssr` para despliegues en modo producción y realizar pruebas SSR(renderización del lado del servidor). 
Navega a `http://localhost:4000/`.

## Lift Fake Rest API (Para simular el servidor con servicios web que consumen ciencuadras-demo/db.json)

Cambiar a directorio -> cd ciencuadras-demo 
Ejecutar -> npm run json-server

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


